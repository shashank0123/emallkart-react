import {combineReducers} from 'redux';
// import ApiReducer from './api/reducer';
import ScreenReducer from './screens/reducer';

const appReducer = combineReducers({
  // api: ApiReducer,
  screens: ScreenReducer,
});

const rootReducer = (state, action) => {
  switch (action.type) {
    case 'ACCOUNT_POST_LOGOUT':
      return appReducer(undefined, action);
    case 'ACCOUNT_POST_DELETE_USER':
      return appReducer(undefined, action);
    case 'PROCESS_BACK_BUTTON_DEVICES':
      return appReducer(undefined, action);
    default:
      return appReducer(state, action);
  }
};

export default rootReducer;
