import React from 'react';
import {View} from 'react-native';
import {applyMiddleware, createStore, compose} from 'redux';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import Reducers from './reducer';
import Navigator from './navigator';
import Reactotron from './ReactotronConfig';

import Sagas from './sagas';

/* global __DEV__ */
let store = null;
let sagaMiddleware = null;
// create the saga middleware
/** @format */
if (__DEV__) {
  sagaMiddleware = createSagaMiddleware();
  store = createStore(
    Reducers,
    compose(
      applyMiddleware(ReduxThunk, sagaMiddleware),
      Reactotron.connect().createEnhancer(),
    ),
  );
} else {
  sagaMiddleware = createSagaMiddleware();
  store = createStore(
    Reducers,
    {},
    applyMiddleware(ReduxThunk, sagaMiddleware),
  );
}

// then run the saga
sagaMiddleware.run(Sagas);

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{flex: 1, backgroundColor: 'red'}}>
          {/* <NavigationContainer> */}
          <Navigator />
          {/* </NavigationContainer> */}
        </View>
      </Provider>
    );
  }
}

export default App;
