import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import HeaderIcon from 'screens/Atoms/HeaderIcon';

import styles from './styles';
import {rateHeight} from 'services/DeviceInfo';

interface Props {
  label: string;
  isShowLabel: boolean;
  isShowRightButton: boolean;
  backClick: () => void;
  actionClick: () => void;
  iconName: string;
}

class Headerbar extends React.PureComponent<Props> {
  public render() {
    const {label, iconName, isShowLabel, isShowRightButton} = this.props;
    const {container, leftView, centerView, rightView} = styles;
    const buttonStyle = {backgroundColor: '#ffffff'};
    const renderLabel = isShowLabel ? (
      <Text style={styles.titleText}>{label}</Text>
    ) : (
      <View />
    );
    const renderRightButton = isShowRightButton ? (
      <HeaderIcon
        name={iconName}
        buttonStyle={buttonStyle}
        iconColor={`black`}
        actionClick={this.props.actionClick}
      />
    ) : (
      <View />
    );
    return (
      <View style={container}>
        <TouchableOpacity style={leftView} onPress={this.props.backClick}>
          <MaterialIcons
            name="chevron-left"
            color="black"
            size={23 * rateHeight}
          />
        </TouchableOpacity>
        <View style={centerView}>{renderLabel}</View>
        <View style={rightView}>{renderRightButton}</View>
      </View>
    );
  }
}

export default Headerbar;
