import React from 'react';
import {Text, View, TouchableOpacity, Animated} from 'react-native';
import {Avatar, Badge, Icon, withBadge} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';
import {rateHeight} from 'services/DeviceInfo';

interface Props {
  label: string;
  data: any;
  isShowLabel: boolean;
  isShowRightButton: boolean;
  backClick: () => void;
  actionClick: () => void;
  iconName: string;
}

class Headerbar extends React.PureComponent<Props> {
  springValue: Animated.Value;
  constructor(props) {
    super(props);
    this.springValue = new Animated.Value(1);
  }

  _getTotalProducts = () => {
    const {data} = this.props;
    let total = 0;
    data.forEach((item) => {
      total += item.amount;
    });
    return total;
  };

  _springAnimation = () => {
    this.springValue.setValue(0.5);
    Animated.spring(this.springValue, {
      toValue: 1,
      friction: 1,
    }).start();
  };

  componentDidUpdate = () => {
    this._springAnimation();
  };
  public render() {
    const {label, isShowLabel, isShowRightButton} = this.props;
    const {container, leftView, centerView, rightView} = styles;
    const {titleText} = styles;

    const renderLabel = isShowLabel ? (
      <Text style={titleText}>{label}</Text>
    ) : (
      <View />
    );
    const renderRightButton = isShowRightButton ? (
      <TouchableOpacity style={styles.buttonView}>
        <MaterialIcons
          name={'shopping-cart'}
          color={'#00C569'}
          size={30 * rateHeight}
        />
        <Animated.View
          style={{
            position: 'absolute',
            top: -4,
            right: -4,
            transform: [
              {
                scale: this.springValue,
              },
            ],
          }}>
          <Badge status="success" value={this._getTotalProducts()} />
        </Animated.View>
      </TouchableOpacity>
    ) : (
      <View />
    );
    return (
      <View style={container}>
        <TouchableOpacity style={leftView} onPress={this.props.backClick}>
          <MaterialIcons
            name="chevron-left"
            color="black"
            size={23 * rateHeight}
          />
        </TouchableOpacity>
        <View style={centerView}>{renderLabel}</View>
        <View style={rightView}>{renderRightButton}</View>
      </View>
    );
  }
}

export default Headerbar;
