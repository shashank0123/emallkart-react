import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 130 * rateHeight,
    justifyContent: 'center',
    paddingTop: 74 * rateHeight,
  },

  leftView: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 15 * rateWidth,
  },

  centerView: {
    flex: 6,
    justifyContent: 'center',
  },

  rightView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  titleText: {
    fontFamily: 'System',
    fontSize: 20 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 24 * rateHeight,
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },

  buttonView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40 * rateWidth,
    height: 40 * rateWidth,
    borderRadius: 45,
    backgroundColor: 'white',
  },
});

export default styles;
