import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 16 * rateWidth,
    justifyContent: 'flex-end',
  },
  searchView: {
    flex: 9,
    marginTop: 74 * rateHeight,
  },

  searchInput: {
    padding: 10 * rateWidth,
    width: 290 * rateWidth,
    height: 40 * rateHeight,
    borderRadius: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
  },

  rightView: {
    flex: 1,
    marginTop: 74 * rateHeight,
  },
});

export default styles;
