import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth, isDeviceIphoneX} from 'services/DeviceInfo';
import IphoneX from './styles.ios.x';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 16 * rateWidth,
    justifyContent: 'flex-end',
  },
  searchView: {
    flex: 9,
    marginTop: 50 * rateHeight,
  },

  searchInput: {
    padding: 10 * rateWidth,
    width: 290 * rateWidth,
    height: 40 * rateHeight,
    borderRadius: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
  },

  rightView: {
    flex: 1,
    marginTop: 50 * rateHeight,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
