import React from 'react';
import {Text, View, ScrollView} from 'react-native';
import HeaderIcon from 'screens/Atoms/HeaderIcon';
import {connect} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SearchInput, {createFilter} from 'react-native-search-filter';
import styles from './styles';
import * as exploreSelectors from 'screens/Pages/Explore/selectors';
import * as exploreActions from 'screens/Pages/Explore/actions';

const KEYS_TO_FILTERS = ['product_name'];
import {products} from 'datafake';
import {rateHeight} from 'services/DeviceInfo';

interface Props {
  isShowRightButton: boolean;
  actionClick: () => void;
  changeKeySearchFilter: (data: string) => void;
  updateProductResult: (data: array) => void;
  iconName: string;
  keySearch: string;
}

class Headerbar extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  searchUpdated(term) {
    // Update key search
    this.props.changeKeySearchFilter(term);

    // Update result
    const productResult = products.filter(createFilter(term, KEYS_TO_FILTERS));
    this.props.updateProductResult(productResult);
  }

  public render() {
    const {iconName, isShowRightButton} = this.props;
    const {container, rightView} = styles;

    const renderRightButton = isShowRightButton ? (
      <HeaderIcon name={iconName} actionClick={this.props.actionClick} />
    ) : (
      <View />
    );
    return (
      <View style={container}>
        <View style={styles.searchView}>
          <SearchInput
            onChangeText={(term) => {
              this.searchUpdated(term);
            }}
            style={styles.searchInput}
            clearIcon={
              this.props.keySearch === '' ? null : (
                <AntDesign name="close" color={'black'} size={20} />
              )
            }
            clearIconViewStyles={{
              position: 'absolute',
              top: 10 * rateHeight,
              right: 30,
            }}
            placeholder="Type a key to search"
          />
        </View>
        <View style={rightView}>{renderRightButton}</View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  keySearch: exploreSelectors.getKeySearch(state),
});

export default connect(mapStateToProps, {...exploreActions})(Headerbar);
