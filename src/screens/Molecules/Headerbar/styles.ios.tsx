import {StyleSheet} from 'react-native';
import {
  rateHeight,
  rateWidth,
  deviceWidth,
  isDeviceIphoneX,
} from 'services/DeviceInfo';
import IphoneX from './styles.ios.x';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    height: 100 * rateHeight,
    justifyContent: 'center',
    paddingTop: 40 * rateHeight,
  },

  leftView: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 15 * rateWidth,
  },

  centerView: {
    flex: 6,
    justifyContent: 'center',
  },

  rightView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  titleText: {
    fontFamily: 'System',
    fontSize: 20 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 24 * rateHeight,
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
