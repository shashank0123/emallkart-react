import {StyleSheet} from 'react-native';
import {rateHeight} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  // start dialog Modal styles
  dialogModalContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  dialogModalView: {
    height: 125 * rateHeight,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10 * rateHeight,
  },

  // body
  bodyView: {
    flex: 2,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bodyText: {
    fontFamily: 'System',
    fontSize: 15 * rateHeight,
    marginVertical: 20 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.64)',
  },

  footerView: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  footerButtonView: {
    width: '60%',
    height: 46 * rateHeight,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  footerButtonViewText: {
    fontFamily: 'Segoe UI',
    fontSize: 20 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    textAlign: 'center',
    color: '#28be2e',
  },
});

export default styles;
