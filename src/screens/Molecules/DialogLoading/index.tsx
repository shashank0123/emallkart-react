import React from 'react';
import {connect} from 'react-redux';
import {View, Text, ActivityIndicator} from 'react-native';
import PropTypes from 'prop-types';
import Dialog, {SlideAnimation} from 'react-native-popup-dialog';
import styles from './styles';

class Index extends React.Component {
  render() {
    const {dialogModalContainer, dialogModalView} = styles;
    const {bodyView, bodyText} = styles;
    const {loading, paymentSucces} = this.props;
    const child = !paymentSucces ? (
      <View style={bodyView}>
        <Text style={bodyText}>{`Waiting for payment...`}</Text>
        <ActivityIndicator size="large" color="#f35588" animating={true} />
      </View>
    ) : (
      <View style={bodyView}>
        <Text style={bodyText}>{`Payment succesful!`}</Text>
      </View>
    );
    return (
      <View style={dialogModalContainer}>
        <Dialog
          visible={loading}
          onTouchOutside={() => this.props.hideDialog()}
          dialogStyle={{width: '70%'}}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: 'bottom',
            })
          }>
          <View style={dialogModalView}>{child}</View>
        </Dialog>
      </View>
    );
  }
}
export default Index;
