import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  isShowLeftButton: boolean;
  textLeftButton: string;
  textRightButton: string;
  actionLeftClick: () => void;
  actionRightClick: () => void;
}

class Main extends React.PureComponent<Props> {
  public render() {
    const {leftView, rightView} = styles;

    const LeftView = this.props.isShowLeftButton ? (
      <TouchableOpacity
        style={leftView}
        onPress={() => this.props.actionLeftClick()}>
        <View style={styles.buttonLeftView}>
          <Text style={styles.textLeftButton}>{this.props.textLeftButton}</Text>
        </View>
      </TouchableOpacity>
    ) : (
      <View style={styles.leftView} />
    );
    return (
      <View style={styles.container}>
        {LeftView}
        <TouchableOpacity
          style={rightView}
          onPress={() => this.props.actionRightClick()}>
          <View style={styles.buttonRightView}>
            <Text style={styles.textButton}>{this.props.textRightButton}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Main;
