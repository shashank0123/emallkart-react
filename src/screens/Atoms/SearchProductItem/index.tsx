import React from 'react';
import {Image, View, TouchableOpacity, Text} from 'react-native';
import styles from './styles';

interface Props {
  data: any;
  search: string;
  actionClick: (data: any) => void;
}

const TextUnderline = (props) => (
  <Text style={[styles.productNameBold]}>{props.children}</Text>
);

class Item extends React.PureComponent<Props> {
  public render() {
    const {container} = styles;
    // TODO format product (this.props.data)
    return (
      <TouchableOpacity
        style={container}
        onPress={() => this.props.actionClick(this.props.data)}>
        <View style={styles.itemView}>
          <Text style={styles.productNameBold}>
            {this.props.data.product_name}
          </Text>
          {/* <Text style={styles.productNameBold}>{this.props.data.cost}</Text> */}
        </View>
      </TouchableOpacity>
    );
  }
}

export default Item;
