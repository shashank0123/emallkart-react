import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth, deviceWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    height: 41 * rateHeight,
  },

  itemView: {
    flex: 1,
    flexDirection: 'row',
  },

  productNameNormal: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 32,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#bebebe',
  },

  productNameBold: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 32,
    letterSpacing: 0,
    textAlign: 'left',
    color: 'black',
  },
});

export default styles;
