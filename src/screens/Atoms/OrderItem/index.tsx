import React from 'react';
import moment from 'moment';
import ImageOrderItem from 'screens/Atoms/ImageOrderItem';
import {Image, View, TouchableOpacity, Text, Animated} from 'react-native';
import styles from './styles';

interface Props {
  data: any;
}

class Main extends React.PureComponent<Props, State> {
  public render() {
    const {container} = styles;
    const {data} = this.props;

    const showDate = moment.unix(data.created_at).format('MM/DD/YYYY');
    const statusStyle =
      data.status === 'Delivered'
        ? styles.orderStatusFinish
        : styles.orderStatusDeliver;
    return (
      <View style={container}>
        <Text style={styles.dateText}>{showDate}</Text>
        <View style={styles.orderItemBox}>
          <View style={styles.orderContentView}>
            <Text style={styles.orderNameText}>{data.order_name}</Text>
            <Text style={styles.orderPriceText}>{`${data.price} $`}</Text>
            <TouchableOpacity
              style={statusStyle}
              onPress={() => this.props.actionClick(data)}>
              <Text style={styles.statusText}>{data.status}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.orderImageView}>
            <ImageOrderItem products={data.products} />
          </View>
        </View>
      </View>
    );
  }
}

export default Main;
