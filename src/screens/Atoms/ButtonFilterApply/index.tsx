import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  cancelAction: () => void;
  acceptAction: () => void;
}

class Main extends React.PureComponent<Props> {
  public render() {
    const {leftView, rightView} = styles;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={leftView}
          onPress={() => this.props.cancelAction()}>
          <View style={styles.buttonLeft}>
            <Text style={styles.textButton2}>{`CLEAR`}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={rightView}
          onPress={() => this.props.acceptAction()}>
          <View style={styles.buttonRight}>
            <Text style={styles.textButton}>{`APPLY`}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Main;
