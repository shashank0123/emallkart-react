import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
  },

  leftView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  rightView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLeft: {
    width: 146,
    height: 50,
    borderRadius: 4,
    borderStyle: 'solid',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#00c569',
  },
  buttonRight: {
    width: 146,
    height: 50,
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: '#00c569',
  },

  textStyle: {
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#929292',
  },

  priceText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 21,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },
  textButton2: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },

  textButton: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#ffffff',
  },
});

export default styles;
