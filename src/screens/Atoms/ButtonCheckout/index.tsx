import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  data: object;
  actionClick: () => void;
}

class Main extends React.PureComponent<Props> {
  totalPrice = () => {
    const {data} = this.props;
    let total = 0;
    data.forEach((element) => {
      total += element.cost * element.amount;
    });
    return total;
  };
  public render() {
    const {leftView, rightView} = styles;

    return (
      <View style={styles.container}>
        <View style={leftView}>
          <Text style={styles.textStyle}>{`TOTAL`}</Text>
          <Text style={styles.priceText}>{`${this.totalPrice()}$`}</Text>
        </View>
        <TouchableOpacity
          style={rightView}
          onPress={() => this.props.actionClick()}>
          <View style={styles.buttonView}>
            <Text style={styles.textButton}>{`CHECKOUT`}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Main;
