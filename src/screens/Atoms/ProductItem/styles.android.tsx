import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 164 * rateWidth,
    // justifyContent: 'space-between',
    alignItems: 'center',
  },

  buttonView: {
    height: 240 * rateHeight,
  },

  buttonImage: {
    height: 240 * rateHeight,
    width: 164 * rateWidth,
    borderRadius: 4,
    // resizeMode: 'contain',
  },

  productNameText: {
    marginTop: 10 * rateHeight,
    width: 164 * rateWidth,
    fontFamily: 'System',
    fontSize: 16 * rateWidth,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: 'black',
  },

  productBrandText: {
    width: 164 * rateWidth,
    marginTop: 2 * rateHeight,
    fontFamily: 'System',
    fontSize: 12 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#929292',
  },

  productCostText: {
    width: 164 * rateWidth,
    marginTop: 5 * rateHeight,
    fontFamily: 'System',
    fontSize: 16 * rateHeight,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },
});

export default styles;
