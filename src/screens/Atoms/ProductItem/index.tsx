import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import styles from './styles';

interface Product {
  product_image: string;
  product_name: string;
  brand_name: string;
  product_price: number;
}

interface Props {
  data: Product;
  actionClick: (data: Product) => void;
}

class Product extends React.PureComponent<Props> {
  public render() {
    const {buttonView} = styles;
    const {data} = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.actionClick(data)}>
        <View style={buttonView}>
          <Image source={{uri: data.product_image}} style={styles.buttonImage} />
        </View>
        <Text style={styles.productNameText}>{data.product_name}</Text>
        <Text style={styles.productBrandText}>{data.brand_name}</Text>
        <Text style={styles.productCostText}>{`${data.product_price}$`}</Text>
      </TouchableOpacity>
    );
  }
}

export default Product;
