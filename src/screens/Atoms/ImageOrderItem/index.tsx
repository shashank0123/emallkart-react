import React from 'react';
import {Image, View, TouchableOpacity, Text, FlatList} from 'react-native';
import styles from './styles';

interface Props {
  products: any;
}

class Main extends React.PureComponent<Props, State> {
  moreImage = () => {
    const {products} = this.props;
    return (
      <View style={styles.moreView}>
        <Text style={styles.moreText}>{`+${products.length}`}</Text>
      </View>
    );
  };

  imageItem = (url: any) => {
    return <Image source={url} style={styles.image} />;
  };

  getImages = () => {
    let arrays = [];
    this.props.products.forEach((element, index) => {
      if (index > 2) return;
      arrays.push(this.imageItem(element.image));
    });
    arrays.push(this.moreImage());
    return arrays;
  };

  public render() {
    const {container} = styles;

    return (
      <View style={container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          renderItem={({item: rowData, index}) => {
            return rowData;
          }}
          keyExtractor={(item, index) => index}
          columnWrapperStyle={{
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}
          data={this.getImages()}
          numColumns={2}
        />
      </View>
    );
  }
}

export default Main;
