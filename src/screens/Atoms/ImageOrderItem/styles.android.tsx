import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    height: 140 * rateHeight,
    paddingVertical: 20 * rateHeight,
    paddingHorizontal: 15,
  },

  moreView: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 4,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e1e1e1',
  },
  moreText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#000000',
  },
  image: {
    width: 50,
    height: 50,
    margin: 2,
    borderRadius: 4,
  },
});

export default styles;
