import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
  },

  leftView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  rightView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonView: {
    width: 146,
    height: 50,
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: '#00c569',
  },

  textStyle: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },

  textButton: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#ffffff',
  },
});

export default styles;
