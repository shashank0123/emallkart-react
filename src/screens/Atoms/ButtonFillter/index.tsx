import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  actionClick: () => void;
}

class CourseItem extends React.PureComponent<Props> {
  public render() {
    const {leftView, rightView} = styles;
    return (
      <View style={styles.container}>
        <View style={leftView}>
          <Text style={styles.textStyle}>{`No filters applied`}</Text>
        </View>
        <TouchableOpacity
          style={rightView}
          onPress={() => this.props.actionClick()}>
          <View style={styles.buttonView}>
            <Text style={styles.textButton}>{`FILTER`}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default CourseItem;
