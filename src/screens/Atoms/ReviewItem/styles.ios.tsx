import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  avatarView: {
    flex: 1,
  },

  avatarImage: {
    width: 50 * rateWidth,
    height: 50 * rateWidth,
    borderRadius: 50 * rateHeight,
  },
  // right
  reviewContentView: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },

  reviewUserNameView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  reviewRateView: {
    flex: 1,
    alignItems: 'flex-end',
  },

  reviewUserText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 27,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  reviewContentText: {
    marginTop: 5,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 27,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },
});

export default styles;
