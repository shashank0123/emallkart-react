import React from 'react';
import {Image, View, TouchableOpacity, Text, FlatList} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';

interface Review {
  image: any;
  reviewer_name: string;
  review_content: string;
}
interface Props {
  data: Review;
  actionClick: (data: Review) => void;
}

class BrandItem extends React.PureComponent<Props> {
  public render() {
    const {container} = styles;
    const {data} = this.props;
    if (data === undefined) return <View />;
    return (
      <TouchableOpacity
        style={container}
        onPress={() => this.props.actionClick(data)}>
        <View style={styles.avatarView}>
          <Image source={data.image} style={styles.avatarImage} />
        </View>
        <View style={styles.reviewContentView}>
          <View style={styles.reviewUserNameView}>
            <Text style={styles.reviewUserText}>{data.reviewer_name}</Text>
            <View style={styles.reviewRateView}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={[1, 2, 3, 4, 5]}
                renderItem={({item: rowData}) => {
                  return <AntDesign name="star" color="#ebe300" size={20} />;
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>

          <Text style={styles.reviewContentText}>
            {`${data.review_content}`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default BrandItem;
