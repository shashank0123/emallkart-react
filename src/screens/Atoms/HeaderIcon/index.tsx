import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {rateHeight} from 'services/DeviceInfo';

interface Props {
  name: string;
  buttonStyle: object;
  actionClick: () => void;
}

class CourseItem extends React.PureComponent<Props> {
  public render() {
    const {buttonView} = styles;
    const {buttonStyle, iconColor} = this.props;
    const color = iconColor ? iconColor : 'white';
    return (
      <TouchableOpacity
        style={[buttonView, buttonStyle]}
        onPress={this.props.actionClick}>
        <AntDesign
          name={this.props.name}
          color={color}
          size={20 * rateHeight}
        />
      </TouchableOpacity>
    );
  }
}

export default CourseItem;
