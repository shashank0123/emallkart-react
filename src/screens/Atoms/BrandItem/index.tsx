import React from 'react';
import {Image, View, TouchableOpacity, Text} from 'react-native';
import styles from './styles';

interface Props {
  data: object;
  actionClick: () => void;
}

class BrandItem extends React.PureComponent<Props> {
  public render() {
    const {container} = styles;
    const {data} = this.props;
    if (data === undefined) return <View />;
    return (
      <TouchableOpacity
        style={container}
        onPress={() => this.props.actionClick(this.props.data)}>
        <View style={styles.brandImageView}>
          <Image source={this.props.data.image} style={styles.brandImage} />
        </View>
        <View style={styles.brandContentView}>
          <Text style={styles.brandNameText}>{this.props.data.brand_name}</Text>
          <Text style={styles.brandProductTotalText}>
            {`${this.props.data.product_total} Products`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default BrandItem;
