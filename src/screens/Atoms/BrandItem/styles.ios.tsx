import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    marginRight: 10 * rateWidth,
    flexDirection: 'row',
    width: 180 * rateWidth,
    height: 80 * rateHeight,
    borderColor: colors.backgroundDark,
    borderWidth: 0.2,
    borderRadius: 4,
    backgroundColor: 'rgba(36, 36, 36, 0.05)',
    shadowColor: 'rgba(36, 36, 36, 0.05)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 15,
    shadowOpacity: 1,
  },

  brandImageView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  brandImage: {
    resizeMode: 'contain',
  },
  // right
  brandContentView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },

  brandNameText: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  brandProductTotalText: {
    marginTop: 5,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#929292',
  },
});

export default styles;
