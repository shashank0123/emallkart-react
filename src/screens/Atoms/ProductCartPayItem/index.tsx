import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Product {
  image: ImageSourcePropType;
  product_name: string;
  brand: string;
  cost: number;
}

interface Props {
  data: Product;
  actionClick: (data: Product) => void;
}

class Product extends React.PureComponent<Props> {
  public render() {
    const {buttonView} = styles;
    const {data} = this.props;
    const subName =
      data.product_name.length > 14
        ? `${data.product_name.substring(0, 12)}...`
        : data.product_name;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.actionClick(data)}>
        <View style={buttonView}>
          <Image source={data.image} style={styles.buttonImage} />
        </View>
        <Text style={styles.productNameText}>{subName}</Text>

        <Text style={styles.productCostText}>{`${data.cost}$`}</Text>
      </TouchableOpacity>
    );
  }
}

export default Product;
