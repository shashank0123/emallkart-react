import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 7 * rateWidth,
  },

  buttonView: {
    borderRadius: 4,
  },

  buttonImage: {
    width: 120,
    height: 120,
    borderRadius: 5,
  },

  productNameText: {
    width: 120 * rateWidth,
    fontFamily: 'System',
    fontSize: 16 * rateWidth,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: 'black',
  },

  productCostText: {
    width: 120 * rateWidth,
    marginTop: 5 * rateHeight,
    fontFamily: 'System',
    fontSize: 16 * rateHeight,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },
});

export default styles;
