import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  data: object;
  actionClick: () => void;
}

class Main extends React.PureComponent<Props> {
  public render() {
    const {leftView, rightView} = styles;
    const {data} = this.props;
    return (      
      <View style={styles.container}>
        <View style={leftView}>
          <Text style={styles.textStyle}>{`PRICE`}</Text>
          <Text style={styles.priceText}>{`${data.product_price}$`}</Text>
        </View>
        <TouchableOpacity
          style={rightView}
          onPress={() => this.props.actionClick()}>
          <View style={styles.buttonView}>
            <Text style={styles.textButton}>{`ADD`}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Main;
