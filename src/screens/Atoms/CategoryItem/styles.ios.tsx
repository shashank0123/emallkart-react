import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20 * rateHeight,
  },

  buttonView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20 * rateWidth,
    width: 60 * rateWidth,
    height: 60 * rateWidth,
    borderRadius: 45 * rateHeight,
    backgroundColor: '#ffffff',
    shadowColor: 'rgba(36, 36, 36, 0.05)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 20 * rateHeight,
    shadowOpacity: 1,
    borderWidth: 0.5,
    borderColor: 'rgba(36, 36, 36, 0.05)',
  },

  buttonImage: {
    width: 30 * rateWidth,
    height: 30 * rateWidth,
    resizeMode: 'contain',
  },

  categoryText: {
    marginRight: 20 * rateWidth,
    marginTop: 15 * rateHeight,
    fontFamily: 'System',
    fontSize: 12 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: 'black',
  },
});

export default styles;
