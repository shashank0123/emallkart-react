import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageSourcePropType,
} from 'react-native';
import styles from './styles';

interface Props {
  data: object;
  actionClick: () => void;
}

class CourseItem extends React.PureComponent<Props> {
  public render() {
    const {buttonView} = styles;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.actionClick(this.props.data)}>
        <View style={buttonView}>
          <Image source={{uri:this.props.data.category_image}} style={styles.buttonImage} />
        </View>
        <Text style={styles.categoryText}>{this.props.data.category_name}</Text>
      </TouchableOpacity>
    );
  }
}

export default CourseItem;
