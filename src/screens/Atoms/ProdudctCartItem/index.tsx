import React from 'react';
import {Image, View, TouchableOpacity, Text, Animated} from 'react-native';
import styles from './styles';

interface Props {
  data: any;
  changeAmount: (product: object, increase: boolean) => void;
}

interface State {
  springValue: any;
}

class BrandItem extends React.PureComponent<Props, State> {
  public render() {
    const {container} = styles;
    const {data} = this.props;
    return (
      <View
        style={container}
        // onPress={() => this.props.actionClick(this.props.data)}
      >
        <View style={styles.productImageView}>
          <Image source={this.props.data.image} style={styles.productImage} />
        </View>
        <View style={styles.productContentView}>
          <Text style={styles.productNameText}>
            {this.props.data.product_name}
          </Text>
          <Text style={styles.costText}>{`${this.props.data.cost} $`}</Text>
          <View style={styles.productAmountView}>
            <TouchableOpacity
              style={styles.addView}
              onPress={() => this.props.changeAmount(data, false)}>
              <Text style={styles.amountText}>{`-`}</Text>
            </TouchableOpacity>
            <View>
              <Text
                style={styles.amountText}>{`${this.props.data.amount}`}</Text>
            </View>
            <TouchableOpacity
              style={styles.addView}
              onPress={() => this.props.changeAmount(data, true)}>
              <Text style={styles.amountText}>{`+`}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default BrandItem;
