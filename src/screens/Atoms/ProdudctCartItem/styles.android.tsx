import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 120 * rateWidth,
    borderBottomWidth: 8 * rateHeight,
    borderColor: 'white',
    backgroundColor: 'white',
  },

  productImageView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  productImage: {
    width: 110 * rateWidth,
    height: 110 * rateWidth,
    borderRadius: 4,
  },
  // right
  productContentView: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },

  productNameText: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  costText: {
    marginTop: 5,
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 19,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },

  productAmountView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10 * rateHeight,
    // paddingHorizontal: 15 * rateWidth,
    alignItems: 'center',
    width: 95 * rateWidth,
    height: 30 * rateHeight,
    borderRadius: 4,
    backgroundColor: 'rgba(0, 0, 0, 0.06)',
  },

  subView: {
    justifyContent: 'center',
  },
  addView: {justifyContent: 'center', flex: 1},
  amountText: {
    fontFamily: 'System',
    fontSize: 16 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },
});

export default styles;
