interface State {
  [KEY: string]: any;
}
// dialog
export const getDemo = (state: State) => state.screens.page.top.demo;
