import * as actionType from './actionTypes';

const INITIAL_STATE = {
  demo: '',
};

const MainReducer = (state = INITIAL_STATE, action: any) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case actionType.ACTION_DEMO:
      break;

    default:
      return state;
  }
  return newState;
};

export default MainReducer;
