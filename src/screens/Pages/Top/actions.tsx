import * as actionTypes from './actionTypes';

// dialog
export const actionDemo = (data: number) => {
  return {
    type: actionTypes.ACTION_DEMO,
    data,
  };
};
