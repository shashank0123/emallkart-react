import React from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
// Select Templete A example
import HomeTemplate from 'screens/Templetes/Home';

interface Props {
  screenProps: any;
}

class Main extends React.Component<Props> {
  public render() {
    const headerComponent = <View style={{backgroundColor: 'blue'}} />;
    const bodyComponent = <View style={{backgroundColor: 'red'}}></View>;
    const bottomComponent = <View style={{backgroundColor: 'red'}}></View>;
    return (
      <HomeTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {})(Main);
