import React from 'react';
import {connect} from 'react-redux';
import {Text, View, ScrollView} from 'react-native';
import Headerbar from 'screens/Molecules/Headerbar';
// Select Templete A example
import ExploreTemplate from 'screens/Templetes/Explore';
import Body from './body';
import * as selectors from '../selectors';

interface Props {}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  headerButtonAction = () => {
    // TODO
  };
  public render() {
    const header = {
      actionClick: this.headerButtonAction,
      backClick: this.backClick,
      iconName: 'staro',
      isShowRightButton: false,
      isShowLabel: true,
      label: 'Write Review',
    };
    const headerComponent = <Headerbar {...header} />;
    const bottomComponent = <View />;
    const bodyComponent = <Body {...this.props} />;
    return (
      <ExploreTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {})(Main);
