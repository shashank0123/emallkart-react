import React from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './styles';
import Form from './form';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        <Form data={this.props.product} {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
