import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight, deviceWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    marginBottom: 60 * rateHeight,
  },

  form: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 32 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
  },

  rateView: {
    flex: 1,
    marginTop: 50 * rateHeight,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  rateIcon: {
    flex: 1,
    marginHorizontal: 10 * rateWidth,
  },

  productNameText: {
    fontFamily: 'System',
    fontSize: 26,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // TextReivew Write
  writeTextView: {
    // flex: 1,
    marginTop: 50 * rateHeight,
  },

  textReview: {
    marginTop: 20 * rateHeight,
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    borderBottomColor: colors.backgroundDark,
    borderBottomWidth: 1,
    lineHeight: 25 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // Add bottom
  buttonView: {
    marginTop: 50 * rateHeight,
    width: 146,
    height: 50,
    borderRadius: 4,
    backgroundColor: '#00c569',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  buttonText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#ffffff',
  },
});

export default styles;
