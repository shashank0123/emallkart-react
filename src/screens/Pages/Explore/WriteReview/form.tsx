import React from 'react';
import {Text, View, FlatList, TextInput} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import colors from 'constants/colors';

interface State {
  rateValue: number;
  text: string;
}
class Body extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      rateValue: 0,
      text: '',
    };
  }

  addReview = () => {
    // TODO
    const {navigation} = this.props;
    navigation.goBack();
  };

  public render() {
    const {data} = this.props;
    const colorNotReview = colors.backgroundDark;
    const colorReview = '#ebe300';
    return (
      <View style={styles.form}>
        <Text style={styles.productNameText}>{data.product_name}</Text>
        <View style={styles.rateView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={[1, 2, 3, 4, 5]}
            renderItem={({item: rowData}) => {
              const {rateValue} = this.state;
              return (
                <TouchableOpacity
                  onPress={() => this.setState({rateValue: rowData + 1})}>
                  <AntDesign
                    name="star"
                    color={rowData < rateValue ? colorReview : colorNotReview}
                    size={45}
                    style={styles.rateIcon}
                  />
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
        <View style={styles.writeTextView}>
          <TextInput
            style={styles.textReview}
            placeholder="Tell us  your experience"
            multiline={true}
            // numberOfLines={10}
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
          />
        </View>
        {/* Button add */}
        <TouchableOpacity style={styles.buttonView} onPress={this.addReview}>
          <Text style={styles.buttonText}>{`SEND`}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Body;
