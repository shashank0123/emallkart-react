import * as actionTypes from './actionTypes';

// dialog
export const chooseCategoryItem = (data: object) => {
  return {
    type: actionTypes.CHOOSE_CATEGORY_ITEM,
    data,
  };
};

export const chooseBrandItem = (data: object) => {
  return {
    type: actionTypes.CHOOSE_BRAND_ITEM,
    data,
  };
};

export const chooseProductItem = (data: object) => {
  return {
    type: actionTypes.CHOOSE_PRODUCT_ITEM,
    data,
  };
};

export const chooseFilterBrand = (data: object) => {
  return {
    type: actionTypes.CHOOSE_FILTER_BRAND,
    data,
  };
};

export const changeKeySearchFilter = (data: object) => {
  return {
    type: actionTypes.CHANGE_KEY_SEARCH,
    data,
  };
};

export const clearKeySearchFilter = () => {
  return {
    type: actionTypes.CHANGE_KEY_SEARCH,
  };
};

export const updateProductResult = (data: object) => {
  return {
    type: actionTypes.UPDATE_PRODUCT_RESULT,
    data,
  };
};
export const takePictureCamera = (data: object) => {
  return {
    type: actionTypes.TAKE_CAMERA_IMAGE,
    data,
  };
};
