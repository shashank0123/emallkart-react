import * as actionType from './actionTypes';
import {brands} from 'datafake';

const INITIAL_STATE = {
  category: {},
  brand: {},
  product: {},
  brands: brands,

  keySearch: '',
  resultProducts: [],

  cameraUrl: '',
};

const assignBrandSelected = (brands, brand) => {
  return brands.map((item) => {
    if (item.id === brand.id) {
      item.is_selected = !item.is_selected;
      return item;
    }
    return item;
  });
};

const MainReducer = (state = INITIAL_STATE, action: any) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case actionType.CHOOSE_CATEGORY_ITEM:
      newState.category = action.data;
      break;
    case actionType.CHOOSE_BRAND_ITEM:
      newState.brand = action.data;
      break;
    case actionType.CHOOSE_PRODUCT_ITEM:
      newState.product = action.data;
      break;
    case actionType.CHOOSE_FILTER_BRAND:
      newState.brands = [...assignBrandSelected(newState.brands, action.data)];
      break;
    case actionType.CHANGE_KEY_SEARCH:
      newState.keySearch = action.data;
      break;
    case actionType.CLEAR_KEY_SEARCH:
      newState.keySearch = '';
      break;
    case actionType.UPDATE_PRODUCT_RESULT:
      newState.resultProducts = action.data;
      break;
    case actionType.TAKE_CAMERA_IMAGE:
      newState.cameraUrl = action.data;
      break;
    default:
      return state;
  }
  return newState;
};

export default MainReducer;
