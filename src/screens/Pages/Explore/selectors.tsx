interface State {
  [KEY: string]: any;
}
// dialog
export const getCategory = (state: State) =>
  state.screens.page.explore.category;
export const getBrand = (state: State) => state.screens.page.explore.brand;
export const getProduct = (state: State) => state.screens.page.explore.product;
export const getBrands = (state: State) => state.screens.page.explore.brands;

export const getKeySearch = (state: State) =>
  state.screens.page.explore.keySearch;
export const getResultProducts = (state: State) =>
  state.screens.page.explore.resultProducts;
