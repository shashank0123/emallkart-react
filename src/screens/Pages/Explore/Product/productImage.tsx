import React from 'react';
import {Text, View, Image} from 'react-native';

import styles from './styles';

// const image = require('../../../../../assets/images/products/image.png');

class Body extends React.PureComponent<Props> {
  public render() {
    const {data} = this.props;
    return (
      <View style={styles.imageView}>
        <Image source={{uri:data.product_image}} style={styles.productImage} />
        {/* <Image source={image} style={styles.productImage} /> */}
      </View>
    );
  }
}

export default Body;
