import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight, deviceWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    marginBottom: 60 * rateHeight,
  },

  imageView: {
    width: deviceWidth,
    height: 477 * rateHeight,
  },

  productImage: {
    width: deviceWidth,
    height: 477 * rateHeight,
    resizeMode: 'stretch',
  },

  // Product infor

  productInforView: {
    flex: 1,
    paddingHorizontal: 16 * rateWidth,
  },

  productNameText: {
    marginTop: 20 * rateHeight,
    fontFamily: 'System',
    fontSize: 26,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // product size color

  productSizeColor: {
    marginTop: 30 * rateHeight,
    flexDirection: 'row',
  },
  productSize: {
    flex: 1,
    paddingHorizontal: 20 * rateWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 160 * rateWidth,
    height: 40 * rateHeight,
    borderRadius: 25,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#ebebeb',
    flexDirection: 'row',
  },
  blankView: {
    width: 23 * rateWidth,
  },
  productColor: {
    flex: 1,
    paddingHorizontal: 20 * rateWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 160 * rateWidth,
    height: 40 * rateHeight,
    borderRadius: 25,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#ebebeb',
    flexDirection: 'row',
  },

  productSizeText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },
  productSizeValueText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'right',
    color: colors.black,
  },

  productColorView: {
    width: 22 * rateWidth,
    height: 22 * rateWidth,
    borderRadius: 8,
    backgroundColor: '#33427d',
  },

  // product info
  productDetailView: {
    flex: 1,
    marginTop: 40 * rateHeight,
  },

  productDetailText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  productDetailInfoText: {
    marginTop: 20 * rateHeight,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 30,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  productMoreText: {
    marginTop: 10 * rateHeight,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },

  // review product
  reviewView: {
    flex: 1,
    marginTop: 40 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
  },

  reviewTittleText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },
  reviewWriteText: {
    marginTop: 10,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 32,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },

  reviewListView: {
    marginTop: 20 * rateHeight,
  },
});

export default styles;
