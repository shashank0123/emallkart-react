import React from 'react';
import {connect} from 'react-redux';
import {Text, View, ToastAndroid} from 'react-native';
import HeaderFilterbar from 'screens/Molecules/HeaderFilterbar';
import ButtonAddCart from 'screens/Atoms/ButtonAddCart';
// Select Templete A example
import ProductTemplate from 'screens/Templetes/Product';
import Body from './body';
import * as selectors from '../selectors';
import * as cartActions from 'screens/Pages/Cart/actions';

interface Props {
  product: any;
  navigation: any;
  addProductToCart: (data: any) => {};
  favouriteProductFromCart: (data: any) => {};
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  favouriteProducts = () => {
    const {favouriteProductFromCart} = this.props;
    const {product} = this.props;
    favouriteProductFromCart(product);
    ToastAndroid.show('Liked this product!', ToastAndroid.SHORT);

    // TODO
    // Add to server
  };

  addProductToCartAction = () => {
    const {addProductToCart} = this.props;
    const {product} = this.props;
    addProductToCart(product);
    ToastAndroid.show('Product added to cart!', ToastAndroid.SHORT);
    // TODO
    // Add to sesion or server
  };

  public render() {
    const header = {
      actionClick: this.favouriteProducts,
      backClick: this.backClick,
      iconName: 'staro',
      isShowRightButton: true,
      isShowLabel: false,
      label: this.props.product.product_name,
    };
    const headerComponent = <HeaderFilterbar {...header} />;
    const bottomComponent = (
      <ButtonAddCart
        data={this.props.product}
        actionClick={this.addProductToCartAction}
      />
    );
    const bodyComponent = <Body {...this.props} />;
    return (
      <ProductTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {...cartActions})(Main);
