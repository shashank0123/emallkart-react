import React from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './styles';
import ProductImage from './productImage';
import ProductInfo from './productInfo';
import Review from './review';

class Body extends React.PureComponent<Props> {
  public render() {
    console.log("product _info", this.props.product);
    return (
      <ScrollView style={styles.container}>
        <ProductImage data={this.props.product} />
        <ProductInfo data={this.props.product} />
        <Review data={this.props.product} {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
