import React from 'react';
import {Text, View, Image} from 'react-native';
import banner from 'assets/images/banner/banner.png';
import styles from './styles';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.bannerView}>
        <Image source={banner} style={styles.bannerImageView} />
      </View>
    );
  }
}

export default Body;
