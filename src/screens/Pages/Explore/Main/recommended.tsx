import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import ProductItem from 'screens/Atoms/ProductItem';
import styles from './styles';
import * as selectors from '../selectors';
import * as actions from '../actions';

import {products} from 'datafake';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {rateWidth} from 'services/DeviceInfo';
interface State {
  name: string;
}

class Main extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      products: []
    };
  }

  componentDidMount(){
    fetch("http://emallkart.in/api/recommended_product?latitude=28.33&longitude=76.66&shop_id=1", {
      method: 'GET'
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          this.setState({
            products: data.data
          });
        }
    })
    .catch(err => {

    });
  }

  selectProduct = (product: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  showMoreProduct = () => {
    // TODO
    const {navigation} = this.props;

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  public render() {
    return (
      <View style={styles.bestSell}>
        <View style={styles.bestSellTitleView}>
          <Text style={styles.categoryText}>{`Recommended`}</Text>
          <TouchableOpacity onPress={this.showMoreProduct}>
            <Text style={styles.seeAllText}>{`See all`}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.bestSellProductView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.products}
            ItemSeparatorComponent={() => (
              <View style={{width: 8 * rateWidth}} />
            )}
            renderItem={({item: rowData}) => {
              return (
                <ProductItem data={rowData} actionClick={this.selectProduct} />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {...actions})(Main);
