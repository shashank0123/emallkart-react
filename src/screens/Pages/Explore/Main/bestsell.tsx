import React from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import ProductItem from 'screens/Atoms/ProductItem';
import styles from './styles';
import * as selectors from '../selectors';
import * as actions from '../actions';
// import {products} from 'datafake';
import {rateWidth} from 'services/DeviceInfo';

interface State {
  name: string;
}

class Main extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  componentDidMount(){
    fetch("http://emallkart.in/api/best_selling?latitude=28.33&longitude=76.66&shop_id=1", {
      method: 'GET'
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          this.setState({
            products: data.data
          });
        }
    })
    .catch(err => {

    });
  }

  selectProduct = (product: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  showMoreProduct = () => {
    // TODO
    const {navigation} = this.props;

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  public render() {
    return (
      <View style={styles.bestSell}>
        <View style={styles.bestSellTitleView}>
          <Text style={styles.categoryText}>{`Best Selling`}</Text>
          <TouchableOpacity onPress={this.showMoreProduct}>
            <Text style={styles.seeAllText}>{`See all`}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.bestSellProductView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={() => (
              <View style={{width: 8 * rateWidth}} />
            )}
            data={this.state.products}
            renderItem={({item: rowData}) => {
              return (
                <ProductItem data={rowData} actionClick={this.selectProduct} />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {...actions})(Main);
