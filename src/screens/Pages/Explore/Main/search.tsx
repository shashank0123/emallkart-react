import React from 'react';
import {
  Text,
  View,
  FlatList,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import SearchProductItem from 'screens/Atoms/SearchProductItem';
import styles from './styles';
import Recommended from './recommended';
import * as actions from '../actions';
import * as selectors from '../selectors';
import {ScrollView} from 'react-native-gesture-handler';

const history = [
  'Shoes',
  'Sport wears',
  'Caps',
  'Bang',
  'Mackbook',
  'Apple',
  'Sport',
  'Books',
];
interface State {
  name: string;
}

class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  selectProductSearch = (product: object) => {
    // TODO
    // Show product
    ToastAndroid.show('Selected product.', ToastAndroid.SHORT);
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  searchWithHistory = (key: string) => {
    // TODO
    // Show product
    const {navigation} = this.props;

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  public render() {
    return (
      <ScrollView style={styles.searchView}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={this.props.resultProducts}
          renderItem={({item: rowData}) => {
            return (
              <SearchProductItem
                data={rowData}
                search={this.props.keySearch}
                actionClick={this.selectProductSearch}
              />
            );
          }}
          keyExtractor={(item, index) => index}
        />
        {/* show history search */}
        <View style={styles.searchRecentView}>
          <Text style={styles.brandText}>{`Recent Searches`}</Text>
          <FlatList
            data={history}
            renderItem={({item: rowData}) => {
              return (
                <TouchableOpacity
                  onPress={this.searchWithHistory}
                  style={styles.historyItemSearchView}>
                  <View>
                    <Text style={styles.historySearchText}>{rowData}</Text>
                  </View>
                </TouchableOpacity>
              );
            }}
            keyExtractor={({key}) => key}
            style={{marginTop: 30}}
            contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap', //Needed for wrapping for the items
            }}
          />
        </View>
        {/* RecomendedProduct */}
        <Recommended {...this.props} />
      </ScrollView>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  resultProducts: selectors.getResultProducts(state),
  keySearch: selectors.getKeySearch(state),
});

export default connect(mapStateToProps, {...actions})(Form);
