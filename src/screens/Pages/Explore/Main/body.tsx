import React from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './styles';
import Category from './category';
import BestSell from './bestsell';
import BannerView from './banner';
import BrandView from './brand';
import Recommended from './recommended';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        <Category {...this.props} />
        <BestSell {...this.props} />
        <BannerView />
        <BrandView {...this.props} />
        <Recommended {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
