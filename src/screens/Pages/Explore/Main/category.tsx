import React from 'react';
import {connect} from 'react-redux';
import {Text, View, FlatList} from 'react-native';
import * as Routes from 'navigator/routerName';
import CategoryItem from 'screens/Atoms/CategoryItem';
import styles from './styles';
import * as actions from '../actions';
import {categories} from 'datafake';

interface State {
  name: string;
}

class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      categories: []
    };
  }

  componentDidMount(){
    fetch("http://emallkart.in/api/categories?latitude=28.33&longitude=76.66&shop_id=1",
    {
      method: 'GET'
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          this.setState({
            categories: data.data
          });
        }
    })
    .catch(err => {

    });
  }

  selectCategory = (category: object) => {
    // TODO
    const {chooseCategoryItem} = this.props;
    chooseCategoryItem(category);
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_CATEGORY,
    });
  };

  public render() {
    return (
      <View style={styles.category}>
        <Text style={styles.categoryText}>{`Categories`}</Text>
        <View style={styles.categoryItemView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.categories}
            renderItem={({item: rowData}) => {
              return (
                <CategoryItem
                  data={rowData}
                  actionClick={this.selectCategory}
                />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {...actions})(Form);
