import React from 'react';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import HeaderSearchBar from 'screens/Molecules/HeaderSearchBar';

import Explore from 'screens/Templetes/Explore';
import Body from './body';
import Search from './search';
import * as selectors from '../selectors';
import * as actions from '../actions';

interface Props {
  keySearch: string;
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  showCamera = () => {
    // TODO
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_CAMERA,
    });
  };
  public render() {
    const {keySearch} = this.props;
    const header = {
      actionClick: this.showCamera,
      iconName: 'camera',
      isShowRightButton: true,
    };
    const headerComponent = <HeaderSearchBar {...header} />;
    const bodyComponent =
      keySearch === '' ? <Body {...this.props} /> : <Search {...this.props} />;
    return (
      <Explore
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  keySearch: selectors.getKeySearch(state),
});

export default connect(mapStateToProps, actions)(Main);
