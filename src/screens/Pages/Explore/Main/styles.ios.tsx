import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16 * rateWidth,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },

  category: {
    marginTop: 20 * rateHeight,
    flexDirection: 'column',
    height: 130 * rateHeight,
    backgroundColor: 'white',
  },

  categoryItemView: {
    flex: 1,
  },

  categoryText: {
    // flex: 1,
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // best sell
  bestSell: {
    // flex:
    marginTop: 52 * rateHeight,
    // paddingHorizontal: 16 * rateWidth,
    backgroundColor: 'white',
  },

  seeAllText: {
    marginTop: 5 * rateHeight,
    fontFamily: 'System',
    fontSize: 16 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'right',
    color: colors.black,
  },

  bestSellTitleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  bestSellProductView: {
    marginTop: 32 * rateHeight,
  },

  // Banner for product
  bannerView: {
    marginTop: 20 * rateHeight,
    alignItems: 'center',
    // paddingHorizontal: 20 * rateWidth,
    borderRadius: 4,
  },

  bannerImageView: {
    resizeMode: 'stretch',
  },

  // Brand list
  brand: {
    flex: 1,
    marginTop: 52 * rateHeight,
    // paddingHorizontal: 16 * rateWidth,
    backgroundColor: 'white',
  },

  brandListView: {
    marginTop: 32 * rateHeight,
  },

  brandText: {
    // flex: 1,
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // search view
  searchView: {
    // flex: 1,
    paddingHorizontal: 16 * rateWidth,
  },

  searchRecentView: {
    // flex: 1,
    marginTop: 20 * rateHeight,
  },

  historyItemSearchView: {
    height: 40,
    marginRight: 8,
    marginVertical: 10,
    alignSelf: 'center',
    paddingHorizontal: 30 * rateWidth,
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
  },

  historySearchText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#000000',
  },
});

export default styles;
