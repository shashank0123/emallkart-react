import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import BrandItem from 'screens/Atoms/BrandItem';
import styles from './styles';
import * as actions from '../actions';

import {brands} from 'datafake';

interface State {
  name: string;
}

class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  selectBrand = (brand: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseBrandItem} = this.props;

    chooseBrandItem(brand);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  public render() {
    return (
      <View style={styles.brand}>
        <Text style={styles.brandText}>{`Featured Brands`}</Text>

        <View style={styles.brandListView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={brands}
            renderItem={({item: rowData}) => {
              return (
                <BrandItem data={rowData} actionClick={this.selectBrand} />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {...actions})(Form);
