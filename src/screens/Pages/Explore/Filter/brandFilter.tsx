import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, View, FlatList} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import {assignArrayToString} from 'ultis';
import styles from './styles';
import * as selectors from '../selectors';
import * as actions from '../actions';
import {rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

interface Props {
  brands: array;
  chooseFilterBrand: (data: object) => void;
}

const SECTIONS = [
  {
    title: 'Brands',
  },
];

class AccordionView extends Component<Props> {
  state = {
    activeSections: [],
  };

  _renderHeader = (section, index, isActive, sections) => {
    const bgView = {
      backgroundColor: isActive ? 'white' : 'white',
    };
    const iconName = isActive ? 'expand-less' : 'expand-more';
    const brandString = assignArrayToString(this.props.brands, 'brand_name');
    const text = brandString === '' ? 'No Setting' : brandString;
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={[styles.headerStyle, bgView]}>
        <View style={styles.headerTextView}>
          <Text style={styles.headerText}>{section.title}</Text>
          <Text style={styles.selectedBrandText}>{text}</Text>
        </View>
        <View style={styles.headerIconView}>
          <MaterialCommunityIcons
            name={iconName}
            size={30 * rateWidth}
            style={styles.iconStyle}
          />
        </View>
      </Animatable.View>
    );
  };

  _renderContent = (section, i, isActive, sections) => {
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={{
          backgroundColor: isActive
            ? 'rgba(255,255,255,1)'
            : 'rgba(255,255,255,1)',
        }}>
        {this._renderListCheckbox()}
      </Animatable.View>
    );
  };

  _updateSections = (activeSections) => {
    this.setState({activeSections});
  };

  _renderListCheckbox = () => {
    return (
      <View style={styles.brandListView}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={this.props.brands}
          renderItem={({item: rowData}) => {
            return this._renderItemCheckbox(rowData);
          }}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  };

  _renderItemCheckbox = (brand) => {
    const {chooseFilterBrand} = this.props;
    return (
      <View style={styles.checkboxContainer}>
        <View style={styles.checkboxRow}>
          <CheckBox
            value={brand.is_selected}
            onValueChange={() => chooseFilterBrand(brand)}
            style={styles.checkbox}
            lineWidth={3}
            hideBox={false}
            boxType={'circle'}
            tintColor={colors.backgroundDark}
            onCheckColor={'#00C569'}
            onFillColor={'white'}
            onTintColor={'#00C569'}
            animationDuration={0.5}
            disabled={false}
            onAnimationType={'bounce'}
            offAnimationType={'stroke'}
          />
          <Text style={styles.label}>{brand.brand_name}</Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <Accordion
        sections={SECTIONS}
        activeSections={this.state.activeSections}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
      />
    );
  }
}
/* @todo using :any */
const mapStateToProps = (state: any) => ({
  brands: selectors.getBrands(state),
});

export default connect(mapStateToProps, {...actions})(AccordionView);
