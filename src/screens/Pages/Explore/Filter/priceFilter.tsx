import React, {Component} from 'react';
import {Text, View, FlatList, CheckBox} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import RangeSlider from 'rn-range-slider';
import styles from './styles';
import {rateWidth, deviceWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

interface Props {}

const SECTIONS = [
  {
    title: 'Price',
  },
];

class AccordionView extends Component<Props> {
  state = {
    activeSections: [],
    rangeLow: 0,
    rangeHigh: 1000,
  };

  _renderHeader = (section, index, isActive, sections) => {
    const {rangeLow, rangeHigh} = this.state;
    const bgView = {
      backgroundColor: isActive ? 'white' : 'white',
    };
    const iconName = isActive ? 'expand-less' : 'expand-more';
    const brandString = `$${rangeLow} - $${rangeHigh}`;
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={[styles.headerStyle, bgView]}>
        <View style={styles.headerTextView}>
          <Text style={styles.headerText}>{section.title}</Text>
          <Text style={styles.selectedBrandText}>{brandString}</Text>
        </View>
        <View style={styles.headerIconView}>
          <MaterialCommunityIcons
            name={iconName}
            size={30 * rateWidth}
            style={styles.iconStyle}
          />
        </View>
      </Animatable.View>
    );
  };

  _renderContent = (section, i, isActive, sections) => {
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={{
          backgroundColor: isActive
            ? 'rgba(255,255,255,1)'
            : 'rgba(255,255,255,1)',
        }}>
        <View style={styles.rangeSliderView}>
          <RangeSlider
            style={{width: 300, height: 80}}
            gravity={'center'}
            min={0}
            max={1000}
            step={20}
            labelStyle="bubble"
            selectionColor="#00c569"
            blankColor={colors.backgroundDark}
            labelBackgroundColor="#00c569"
            labelBorderColor="#00c569"
            onValueChanged={(low, high, fromUser) => {
              this.setState({
                rangeLow: low,
                rangeHigh: high,
              });
            }}
          />
        </View>
      </Animatable.View>
    );
  };

  _updateSections = (activeSections) => {
    this.setState({activeSections});
  };

  render() {
    return (
      <Accordion
        sections={SECTIONS}
        activeSections={this.state.activeSections}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
      />
    );
  }
}

export default AccordionView;
