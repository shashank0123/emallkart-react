import React, {Component} from 'react';
import {Text, View, FlatList} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import {colors} from 'datafake';
import styles from './styles';
import {rateWidth, deviceWidth} from 'services/DeviceInfo';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Props {}

const SECTIONS = [
  {
    title: 'Rate',
  },
];

class AccordionView extends Component<Props> {
  state = {
    activeSections: [],
    rateValue: 0,
  };

  _renderHeader = (section, index, isActive, sections) => {
    const bgView = {
      backgroundColor: isActive ? 'white' : 'white',
    };
    const iconName = isActive ? 'expand-less' : 'expand-more';
    const {rateValue} = this.state;
    const star = rateValue === 0 ? 'No Setting' : `${rateValue} Star`;
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={[styles.headerStyle, bgView]}>
        <View style={styles.headerTextView}>
          <Text style={styles.headerText}>{section.title}</Text>
          <Text style={styles.selectedBrandText}>{star}</Text>
        </View>
        <View style={styles.headerIconView}>
          <MaterialCommunityIcons
            name={iconName}
            size={30 * rateWidth}
            style={styles.iconStyle}
          />
        </View>
      </Animatable.View>
    );
  };

  _renderContent = (section, i, isActive, sections) => {
    const colorNotReview = '#ebebeb';
    const colorReview = '#ebe300';
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={{
          backgroundColor: isActive
            ? 'rgba(255,255,255,1)'
            : 'rgba(255,255,255,1)',
        }}>
        <View style={styles.rateView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={[1, 2, 3, 4, 5]}
            renderItem={({item: rowData}) => {
              const {rateValue} = this.state;
              return (
                <TouchableOpacity
                  onPress={() => this.setState({rateValue: rowData})}>
                  <AntDesign
                    name="star"
                    color={rowData <= rateValue ? colorReview : colorNotReview}
                    size={45}
                    style={styles.rateIcon}
                  />
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </Animatable.View>
    );
  };

  _updateSections = (activeSections) => {
    this.setState({activeSections});
  };

  render() {
    return (
      <Accordion
        sections={SECTIONS}
        activeSections={this.state.activeSections}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
      />
    );
  }
}

export default AccordionView;
