import React, {Component} from 'react';
import {Text, View, FlatList} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import {colors} from 'datafake';
import styles from './styles';
import {rateWidth, deviceWidth} from 'services/DeviceInfo';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Props {}

const SECTIONS = [
  {
    title: 'Color',
  },
];

class AccordionView extends Component<Props> {
  state = {
    activeSections: [],
    color: '#00c569',
  };

  _renderHeader = (section, index, isActive, sections) => {
    const bgView = {
      backgroundColor: isActive ? 'white' : 'white',
    };
    const iconName = isActive ? 'expand-less' : 'expand-more';
    const colorStyle = {backgroundColor: this.state.color};
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={[styles.headerStyle, bgView]}>
        <View style={styles.headerTextView}>
          <Text style={styles.headerText}>{section.title}</Text>
          <View style={[styles.colorSelected, colorStyle]}></View>
        </View>
        <View style={styles.headerIconView}>
          <MaterialCommunityIcons
            name={iconName}
            size={30 * rateWidth}
            style={styles.iconStyle}
          />
        </View>
      </Animatable.View>
    );
  };

  _renderContent = (section, i, isActive, sections) => {
    return (
      <Animatable.View
        duration={300}
        transition="backgroundColor"
        style={{
          backgroundColor: isActive
            ? 'rgba(255,255,255,1)'
            : 'rgba(255,255,255,1)',
        }}>
        <View style={styles.colorView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={colors}
            renderItem={({item: rowData}) => {
              return (
                <TouchableOpacity
                  onPress={() => this.setState({color: rowData.hex})}
                  style={[styles.colorItemView, {backgroundColor: rowData.hex}]}
                />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </Animatable.View>
    );
  };

  _updateSections = (activeSections) => {
    this.setState({activeSections});
  };

  render() {
    return (
      <Accordion
        sections={SECTIONS}
        activeSections={this.state.activeSections}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
      />
    );
  }
}

export default AccordionView;
