import React from 'react';
import {connect} from 'react-redux';
import Headerbar from 'screens/Molecules/Headerbar';
import ButtonFilterApply from 'screens/Atoms/ButtonFilterApply';
import * as Routes from 'navigator/routerName';
import ExploreTemplate from 'screens/Templetes/Explore';
import Body from './body';

interface Props {
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  headerButtonAction = () => {
    // TODO
  };

  acceptFilter = () => {
    // TODO
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  cancelFilter = () => {
    // TODO
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_BRAND,
    });
  };

  public render() {
    const header = {
      actionClick: this.headerButtonAction,
      backClick: this.backClick,
      iconName: 'staro',
      isShowRightButton: false,
      isShowLabel: true,
      label: 'Filter',
    };
    const headerComponent = <Headerbar {...header} />;
    const bottomComponent = (
      <ButtonFilterApply
        acceptAction={this.acceptFilter}
        cancelAction={this.cancelFilter}
      />
    );
    const bodyComponent = <Body {...this.props} />;
    return (
      <ExploreTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  // product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {})(Main);
