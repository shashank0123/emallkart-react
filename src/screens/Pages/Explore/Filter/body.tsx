import React from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './styles';
import BrandFilter from './brandFilter';
import CostFilter from './priceFilter';
import ColorFilter from './colorFilter';
import RateFilter from './rateFilter';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        <BrandFilter {...this.props} />
        <CostFilter {...this.props} />
        <ColorFilter {...this.props} />
        <RateFilter {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
