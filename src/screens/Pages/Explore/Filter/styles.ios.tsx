import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight, deviceWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    paddingLeft: 16 * rateWidth,
    marginBottom: 60 * rateHeight,
  },

  // brand filter
  headerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 80 * rateHeight,
  },

  headerIconView: {
    flex: 2,
    alignItems: 'center',
  },

  headerTextView: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  iconStyle: {
    color: colors.black,
  },

  headerText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 21,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  selectedBrandText: {
    marginTop: 10 * rateHeight,
    opacity: 0.6,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // rang slider
  rangeSliderView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingRight: 16 * rateWidth,
  },

  // color filter
  colorSelected: {
    marginTop: 10,
    width: 20,
    height: 20,
    borderRadius: 5,
    backgroundColor: '#00c569',
  },

  colorView: {
    flex: 1,
    paddingTop: 20 * rateHeight,
  },
  colorItemView: {
    marginHorizontal: 10 * rateWidth,
    width: 35 * rateWidth,
    height: 35 * rateWidth,
    borderRadius: 8,
    backgroundColor: '#00c569',
  },

  // rate filter
  rateView: {
    paddingTop: 20 * rateHeight,
  },

  rateIcon: {
    marginHorizontal: 10 * rateWidth,
  },

  checkboxContainer: {
    flexDirection: 'column',
    padding: 5,
  },

  checkboxRow: {
    flexDirection: 'row',
    // marginVertical: 5,
  },

  label: {
    marginLeft: 10,
    marginTop: 7 * rateHeight,
  },
});

export default styles;
