import React from 'react';
import {Text, View, ScrollView} from 'react-native';

import styles from './styles';
import BrandView from './brand';
import Products from './products';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        {/* <BrandView {...this.props} /> */}
        <Products {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
