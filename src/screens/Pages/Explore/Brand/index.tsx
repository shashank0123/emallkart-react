import React from 'react';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import Headerbar from 'screens/Molecules/Headerbar';
import BottonFillter from 'screens/Atoms/ButtonFillter';
// Select Templete A example
import ExploreTemplate from 'screens/Templetes/Explore';
import Body from './body';
import * as selectors from '../selectors';

interface Props {
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  headerButtonAction = () => {
    // TODO
  };

  filterSetting = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_FILTER,
    });
  };

  public render() {
    const header = {
      actionClick: this.headerButtonAction,
      backClick: this.backClick,
      iconName: 'camera',
      isShowRightButton: false,
      isShowLabel: true,
      label: this.props.brand.brand_name,
    };
    const headerComponent = <Headerbar {...header} />;
    const bottomComponent = <BottonFillter actionClick={this.filterSetting} />;
    const bodyComponent = <Body {...this.props} />;
    return (
      <ExploreTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  brand: selectors.getBrand(state),
});

export default connect(mapStateToProps, {})(Main);
