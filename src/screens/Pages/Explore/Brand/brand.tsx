import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import BrandItem from 'screens/Atoms/BrandItem';
import styles from './styles';
import * as selectors from '../selectors';
import * as actions from '../actions';
import {brands} from 'datafake';

interface State {
  name: string;
}

class Main extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  selectProduct = (product: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  public render() {
    return (
      <View style={styles.brand}>
        <Text style={styles.brandText}>{`Top Brands`}</Text>

        <View style={styles.brandListView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={brands}
            renderItem={({item: rowData}) => {
              return (
                <BrandItem data={rowData} actionClick={this.selectProduct} />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {...actions})(Main);
