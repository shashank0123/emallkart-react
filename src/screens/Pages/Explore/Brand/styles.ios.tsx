import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const MARGIN = 8 * rateWidth;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },

  category: {
    marginTop: 30 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    flexDirection: 'column',
    height: 130 * rateHeight,
    backgroundColor: 'white',
  },

  categoryItemView: {
    flex: 1,
  },

  categoryText: {
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  // best sell
  bestSell: {
    flex: 1,
    marginTop: 15 * rateHeight,
    marginBottom: 60 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    backgroundColor: 'white',
  },

  seeAllText: {
    marginTop: 5 * rateHeight,
    fontFamily: 'System',
    fontSize: 16 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'right',
    color: colors.black,
  },

  bestSellTitleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  bestSellProductView: {
    marginTop: 20 * rateHeight,
  },

  columnWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: -MARGIN,
    marginRight: -MARGIN,
  },

  // Brand list
  brand: {
    flex: 1,
    marginTop: 52 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    backgroundColor: 'white',
  },

  brandListView: {
    marginTop: 32 * rateHeight,
  },

  brandText: {
    // flex: 1,
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },
});

export default styles;
