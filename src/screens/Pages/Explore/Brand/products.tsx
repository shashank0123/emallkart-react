import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import ProductItem from 'screens/Atoms/ProductItem';
import styles from './styles';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import * as selectors from '../selectors';
import * as actions from '../actions';

import {products} from 'datafake';

interface State {
  itemHeight: number;
}

class Main extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      itemHeight: 340 * rateHeight,
    };
  }

  selectProduct = (product: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  _getItemLayout = (data, index) => {
    const {itemHeight} = this.state;
    return {length: itemHeight, offset: itemHeight * index, index};
  };

  public render() {
    const {itemHeight} = this.state;
    return (
      <View style={styles.bestSell}>
        <View style={styles.bestSellTitleView}>
          <Text style={styles.categoryText}>{`List products`}</Text>
        </View>

        <View style={styles.bestSellProductView}>
          <FlatList
            showsVerticalScrollIndicator={false}
            renderItem={({item: rowData}) => {
              return (
                <ProductItem data={rowData} actionClick={this.selectProduct} />
              );
            }}
            keyExtractor={(item, index) => index}
            columnWrapperStyle={[styles.columnWrapper, {height: itemHeight}]}
            data={products}
            numColumns={2}
            scrollEnabled={true}
            getItemLayout={this._getItemLayout}
            onEndReachedThreshold={0.9}
          />
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  product: selectors.getProduct(state),
});

export default connect(mapStateToProps, {...actions})(Main);
