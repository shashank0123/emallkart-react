import {StyleSheet} from 'react-native';
import {rateHeight, rateWidth} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  //
  container: {
    flex: 1,
    backgroundColor: '#333333',
  },

  header: {
    flex: 1,
  },
  content: {
    flex: 10.5,
    flexDirection: 'column',
  },
  body: {
    flex: 7,
    backgroundColor: '#fff',
  },
  // footer: {
  //   flex: 3.5,
  //   backgroundColor: '#333333'
  // },
  // component

  headerContentView: {
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#333333',
  },
  headerIconCloseView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTitleView: {
    flex: 5,
    justifyContent: 'center',
  },
  headerOkIconView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerTittleText: {
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 22 * rateHeight,
    letterSpacing: 0,
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.9)',
  },
  headerTittleOkText: {
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 22 * rateHeight,
    letterSpacing: 0,
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.9)',
  },
  // footer conponent
  footer: {
    flex: 1,
    backgroundColor: colors.transparent,
  },

  footerContentView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },

  // buttons camera

  footerListImageView: {
    flex: 1,
  },
  footerListImageContent: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10 * rateHeight,
    marginLeft: 10 * rateWidth,
  },

  footerItemImageViewSelected: {
    flex: 1,
    width: 75 * rateHeight,
    height: 75 * rateHeight,
    marginRight: 10 * rateWidth,
    backgroundColor: '#666666',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.5)',
  },

  footerItemImageViewNotSelect: {
    flex: 1,
    width: 75 * rateHeight,
    height: 75 * rateHeight,
    marginRight: 10 * rateWidth,
    backgroundColor: '#333333',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.5)',
  },

  footerItemImage: {
    flex: 1,
    alignSelf: 'stretch',
  },
  // footer conponent

  footerListIconView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: colors.transparent,
  },
  footerTabLabelView: {
    flex: 0.5,
    flexDirection: 'column',
  },
  // child
  footerIconView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerIconCameraView: {
    width: 60 * rateHeight,
    height: 60 * rateHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50 * rateHeight,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#707070',
  },
  footerIconCameraChildView: {
    width: 52 * rateHeight,
    height: 52 * rateHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50 * rateHeight,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 3,
    borderColor: '#333333',
  },
  footerIconCamera: {
    marginTop: 5 * rateHeight,
  },
});

export default styles;
