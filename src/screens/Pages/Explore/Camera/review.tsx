import React from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import ReviewItem from 'screens/Atoms/ReviewItem';
import * as Routes from 'navigator/routerName';
import styles from './styles';
import {reviews} from 'datafake';

class Main extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  addReview = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_REVIEW,
    });
  };
  public render() {
    return (
      <View style={styles.reviewView}>
        <Text style={styles.reviewTittleText}>{`Reviews`}</Text>
        <TouchableOpacity onPress={this.addReview}>
          <Text style={styles.reviewWriteText}>{`Write your review`}</Text>
        </TouchableOpacity>

        <View style={styles.reviewListView}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={reviews}
            renderItem={({item: rowData}) => {
              return <ReviewItem data={rowData} actionClick={() => {}} />;
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

export default Main;
