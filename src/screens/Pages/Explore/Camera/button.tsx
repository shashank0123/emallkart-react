import React from 'react';
import {View, TouchableOpacity, ScrollView, Image} from 'react-native';
import {rateHeight} from 'services/DeviceInfo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {FLASH_MODE} from 'constants/index';
import styles from './styles';

class Buttons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderIconFlash = () => {
    const {flashMode} = this.props;
    const isTurnOnFlash = flashMode === FLASH_MODE.ON;
    if (isTurnOnFlash) {
      return (
        <MaterialCommunityIcons
          name="flash"
          size={28 * rateHeight}
          color="#fff"
        />
      );
    }
    return (
      <MaterialCommunityIcons
        name="flash-off"
        size={28 * rateHeight}
        color="#fff"
      />
    );
  };

  render() {
    const {footerListIconView} = styles;
    const {
      footerIconView,
      footerIconCameraView,
      footerIconCameraChildView,
      footerIconCamera,
    } = styles;
    const {takePicture, toggleFlash, toggleFacing} = this.props;

    return (
      <View style={footerListIconView}>
        <TouchableOpacity onPress={toggleFlash} style={footerIconView}>
          {this.renderIconFlash()}
        </TouchableOpacity>
        <View style={footerIconView}>
          <TouchableOpacity onPress={takePicture} style={footerIconCameraView}>
            <View style={footerIconCameraChildView}>
              <MaterialCommunityIcons
                name="camera"
                size={32 * rateHeight}
                color="#333333"
                style={footerIconCamera}
              />
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={toggleFacing} style={footerIconView}>
          <MaterialCommunityIcons
            name="camera-switch"
            size={28 * rateHeight}
            color="#fff"
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default Buttons;
