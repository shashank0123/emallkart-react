import React from 'react';
import {View, TouchableOpacity, Icon} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import {RNCamera} from 'react-native-camera';
import {FLASH_MODE, TYPE_MODE} from 'constants/index';
import Buttons from './button';

import styles from './styles';
import * as actions from '../actions';

class Camera extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flashMode: FLASH_MODE.OFF,
      type: TYPE_MODE.BACK,
    };
  }

  takePicture = async () => {
    const {takePictureCamera} = this.props;
    if (this.camera) {
      const options = {quality: 1, base64: true};
      const data = await this.camera.takePictureAsync(options);
      takePictureCamera(data.uri);

      // TODO
      // AI to search product with image

      // Direct to product list
      const {navigation} = this.props;
      navigation.navigate(Routes.EXPRORE, {
        screen: Routes.EXPRORE_BRAND,
      });
    }
  };

  toggleFlash = () => {
    const {flashMode} = this.state;
    this.setState({
      flashMode: flashMode === FLASH_MODE.ON ? FLASH_MODE.OFF : FLASH_MODE.ON,
    });
  };

  toggleFacing = () => {
    const {type} = this.state;
    this.setState({
      type: type === TYPE_MODE.BACK ? TYPE_MODE.FRONT : TYPE_MODE.BACK,
    });
  };

  render() {
    const {content, body} = styles;
    const {footer, footerContentView} = styles;
    // const { footerTabLabelView } = styles
    const {flashMode, type} = this.state;
    return (
      <View style={content}>
        <View style={body}>
          <RNCamera
            ref={(ref) => {
              this.camera = ref;
            }}
            style={{
              flex: 1,
              justifyContent: 'space-between',
            }}
            androidCameraPermissionOptions={{
              title: 'Cho phép truy cập vào máy ảnh',
              message: 'Ứng dụng cần quyền truy cập đên máy ảnh của bạn',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            type={type}
            flashMode={flashMode}
            autoFocus="off"
            ratio="16:9"
            permissionDialogTitle="Cho phép truy cập vào máy ảnh"
            permissionDialogMessage="Ứng dụng cần quyền truy cập đên máy ảnh của bạn"
          />
        </View>
        <View style={footer}>
          <View style={footerContentView}>
            <Buttons
              flashMode={flashMode}
              takePicture={this.takePicture}
              toggleFlash={this.toggleFlash}
              toggleFacing={this.toggleFacing}
            />
            {/* <View style={footerTabLabelView} /> */}
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {...actions})(Camera);
