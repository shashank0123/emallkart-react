import React from 'react';
import {Text, View, Image} from 'react-native';
import ReadMore from 'react-native-read-more-text';
import styles from './styles';

class Body extends React.PureComponent<Props> {
  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={styles.productMoreText} onPress={handlePress}>
        Read more
      </Text>
    );
  };

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={styles.productMoreText} onPress={handlePress}>
        Show less
      </Text>
    );
  };
  _handleTextReady = () => {
    // ...
  };
  public render() {
    const {data} = this.props;
    const color = {backgroundColor: data.product_color};

    return (
      <View style={styles.productInforView}>
        <Text style={styles.productNameText}>{data.product_name}</Text>

        <View style={styles.productSizeColor}>
          <View style={styles.productSize}>
            <Text style={styles.productSizeText}>{`Size`}</Text>
            <Text style={styles.productSizeValueText}>{data.product_size}</Text>
          </View>
          <View style={styles.blankView} />
          <View style={styles.productColor}>
            <Text style={styles.productSizeText}>{`Color`}</Text>
            <View style={[styles.productColorView, color]}></View>
          </View>
        </View>

        <View style={styles.productDetailView}>
          <Text style={styles.productDetailText}>{`Detail`}</Text>
          {/* <Text style={styles.productDetailInfoText}>{data.product_info}</Text> */}
          {/* <Text style={styles.productMoreText}>{`Read More`}</Text> */}

          <ReadMore
            numberOfLines={3}
            renderTruncatedFooter={this._renderTruncatedFooter}
            renderRevealedFooter={this._renderRevealedFooter}
            onReady={this._handleTextReady}>
            <Text style={styles.productDetailInfoText}>
              {data.product_info}
            </Text>
          </ReadMore>
        </View>
      </View>
    );
  }
}

export default Body;
