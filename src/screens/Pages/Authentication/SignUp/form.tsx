import React from 'react';
import {Text, View, TouchableOpacity, TextInput, ScrollView, Alert, AsyncStorage} from 'react-native';
import * as Routes from 'navigator/routerName';
import styles from './styles';

interface State {
  email: string;
  password: string;
}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      username: '',
      mobile: ''
    };
  }

  signIn = () => {
    const {navigation} = this.props;
    var formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("email", this.state.email);
    formData.append("password", this.state.password);
    formData.append("user_name", this.state.username);
    formData.append("mobile", this.state.mobile);
    fetch("http://emallkart.in/api/auth/register", {
      method: 'POST',
      body: formData
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          AsyncStorage.setItem(
            'token',
            data.data.token
          );
          navigation.navigate(Routes.EXPRORE, {
            screen: Routes.EXPRORE,
          });       
        }else{
          Alert.alert(
            "Register Failed!",
            JSON.stringify(data.error),
            [
              { text: 'OK', onPress: () => {}}
            ]);
        }
    })
    .catch(err => {
      Alert.alert(
        "Register Failed!",
        JSON.stringify(err),
        [
          { text: 'OK', onPress: () => {}}
        ]);
    });       
  };

  public render() {
    return (
      <ScrollView>
      <View style={styles.form}>
        <View style={styles.formView}>
          <View style={styles.formTitleView}>
            <Text style={styles.formTitleText}>{`Sign Up`}</Text>
          </View>
          {/* name */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Name`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="pipong"
              onChangeText={(text) => this.setState({name: text})}
              value={this.state.name}
            />
          </View>
          {/* Email */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Email`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="pipong@gmail.com"
              onChangeText={(text) => this.setState({email: text})}
              value={this.state.email}
            />
          </View>
          {/* password */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Password`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="********"
              secureTextEntry={true}
              onChangeText={(text) => this.setState({password: text})}
              value={this.state.password}
            />
          </View>
          {/* username */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Username`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="pipong"
              onChangeText={(text) => this.setState({username: text})}
              value={this.state.username}
            />
          </View>
          {/* mobile */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Mobile`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="pipong"
              onChangeText={(text) => this.setState({mobile: text})}
              value={this.state.mobile}
            />
          </View>

          <TouchableOpacity style={styles.butonSignIn} onPress={this.signIn}>
            <Text style={styles.formButtonText}>{`SIGN UP`}</Text>
          </TouchableOpacity>
        </View>
      </View>
      </ScrollView>
    );
  }
}

export default Form;
