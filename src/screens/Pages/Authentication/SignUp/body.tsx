import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import styles from './styles';
import FormView from './form';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.container}>
        <FormView {...this.props} />
      </View>
    );
  }
}

export default Body;
