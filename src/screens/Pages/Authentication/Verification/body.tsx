import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import FormView from './form';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.container}>
        {/* <KeyboardAwareScrollView> */}
        <FormView {...this.props} />
        {/* </KeyboardAwareScrollView> */}
      </View>
    );
  }
}

export default Body;
