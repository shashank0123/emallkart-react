import React from 'react';
import {connect} from 'react-redux';
import HeaderBar from 'screens/Molecules/Headerbar';
// Select Templete A example
import AuthenticationTemplate from 'screens/Templetes/Authentication';
import Body from './body';

interface Props {}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  headerButtonAction = () => {
    // TODO
  };
  public render() {
    const header = {
      label: 'Wellcome',
      backClick: this.backClick,
      actionClick: this.headerButtonAction,
      iconName: 'search1',
    };
    const headerComponent = <HeaderBar {...header} />;
    const bodyComponent = <Body {...this.props} />;
    return (
      <AuthenticationTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {})(Main);
