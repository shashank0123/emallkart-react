import React from 'react';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import * as Routes from 'navigator/routerName';
import styles from './styles';

interface State {
  number1: string;
  number2: string;
  number3: string;
  number4: string;
  number5: string;
  number6: string;
}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      number1: '',
      number2: '',
      number3: '',
      number4: '',
      number5: '',
      number6: '',
    };
  }

  signIn = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.BOTTOM_TAB, {
      screen: Routes.EXPRORE,
    });
  };

  setForcus = (number: number) => {
    switch (number) {
      case 1:
        this.number2.focus();
        break;
      case 2:
        this.number3.focus();
        break;
      case 3:
        this.number4.focus();
        break;
      case 4:
        this.number5.focus();
        break;
      case 5:
        this.number6.focus();
        break;
      case 6:
        this.number6.blur();
        break;
      default:
        break;
    }
  };
  public render() {
    return (
      <View style={styles.form}>
        <View style={styles.formView}>
          <View style={styles.formTitleView}>
            <Text style={styles.formTitleText}>{`Verification`}</Text>
          </View>
          <Text
            style={
              styles.formTitleInputText
            }>{`A 6 - Digit PIN has been sent to your email address, enter it below to continue`}</Text>
          {/* name */}
          <View style={styles.formItemInput}>
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number1 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number1: text}, () => {
                  this.setForcus(1);
                })
              }
              value={this.state.number1}
            />
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number2 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number2: text}, () => {
                  this.setForcus(2);
                })
              }
              value={this.state.number2}
            />
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number3 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number3: text}, () => {
                  this.setForcus(3);
                })
              }
              value={this.state.number3}
            />
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number4 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number4: text}, () => {
                  this.setForcus(4);
                })
              }
              value={this.state.number4}
            />
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number5 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number5: text}, () => {
                  this.setForcus(5);
                })
              }
              value={this.state.number5}
            />
            <TextInput
              style={styles.formInputText}
              keyboardType="numeric"
              ref={(ref) => {
                this.number6 = ref;
              }}
              onChangeText={(text) =>
                this.setState({number6: text}, () => {
                  this.setForcus(6);
                })
              }
              value={this.state.number6}
            />
          </View>

          <TouchableOpacity style={styles.butonSignIn} onPress={this.signIn}>
            <Text style={styles.formButtonText}>{`CONTINUE`}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Form;
