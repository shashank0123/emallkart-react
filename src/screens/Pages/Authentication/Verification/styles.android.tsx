import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },

  form: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  formView: {
    marginTop: 32 * rateHeight,
    width: 344 * rateWidth,
    height: 335.6 * rateHeight,
    paddingHorizontal: 15 * rateWidth,
    borderRadius: 4 * rateHeight,
    backgroundColor: '#ffffff',
    shadowColor: 'rgba(36, 36, 36, 0.05)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 15 * rateHeight,
    shadowOpacity: 1,
  },

  formTitleView: {
    marginTop: 10 * rateHeight,
    flexDirection: 'row',
  },

  formTitleText: {
    fontFamily: 'System',
    fontSize: 30 * rateHeight,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 36 * rateHeight,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },

  formTitleInputText: {
    marginTop: 10 * rateHeight,
    fontFamily: 'System',
    fontSize: 14 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#929292',
  },

  // Input item text
  formItemInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 57 * rateHeight,
  },

  formInputText: {
    fontFamily: 'System',
    fontSize: 15 * rateHeight,
    // backgroundColor: 'red',
    width: 40 * rateWidth,
    height: 40 * rateWidth,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    textAlign: 'center',
    borderWidth: 0.5,
    borderColor: '#dddddd',
  },

  formForgetPasswordText: {
    marginTop: 20 * rateHeight,
    fontSize: 14 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'right',
    color: colors.black,
  },
  butonSignIn: {
    marginTop: 65 * rateHeight,
    justifyContent: 'center',
    height: 50 * rateHeight,
    borderRadius: 4 * rateHeight,
    backgroundColor: '#00c569',
  },

  formButtonText: {
    fontSize: 14 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: '#ffffff',
  },

  bottom: {
    padding: 15 * rateWidth,
    flexDirection: 'column',
  },

  bottomMoreText: {
    fontFamily: 'System',
    fontSize: 18 * rateHeight,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },

  buttonOtherLogin: {
    marginVertical: 10 * rateHeight,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: 50 * rateHeight,
    borderRadius: 4,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#dddddd',
  },

  bottomText: {
    marginLeft: 20 * rateWidth,
    fontFamily: 'System',
    fontSize: 14 * rateHeight,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.black,
  },
});

export default styles;
