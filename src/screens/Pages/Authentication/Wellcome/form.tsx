import React from 'react';
import {Text, View, TouchableOpacity, TextInput, AsyncStorage, Alert} from 'react-native';
import * as Routes from 'navigator/routerName';
import styles from './styles';

interface State {
  email: string;
  password: string;
}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  signUp = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.AUTHENTICATION, {
      screen: Routes.AUTH_SIGNUP,
    });
  };

  forgot = () => {
    var formData = new FormData();
    formData.append("email", this.state.email);
    fetch("http://emallkart.in/api/forgot_password", {
      method: 'POST',
      body: formData
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          Alert.alert(
            "Forgot Password",
            data.data,
            [
              { text: 'OK', onPress: () => {}}
            ]);
        }
    })
    .catch(err => {

    });
  }

  signIn = () => {
    const {navigation} = this.props;

    var formData = new FormData();
    formData.append("email", this.state.email);
    formData.append("password", this.state.password);

    fetch("http://emallkart.in/api/auth/login", {
      method: 'POST',
      body: formData
    })
    .then(res => res.json())
    .then(data => {
        if(data.success == true){
          AsyncStorage.setItem(
            'token',
            data.data.token
          );
          navigation.navigate(Routes.BOTTOM_TAB, {
            screen: Routes.EXPRORE,
          });          
        }else{
          Alert.alert(
            "Login Failed!",
            "Email or Password don't match.",
            [
              { text: 'OK', onPress: () => {}}
            ]);
        }
    })
    .catch(err => {
      Alert.alert(
        "Login Failed!",
        "Please check Network or Wifi.",
        [
          { text: 'OK', onPress: () => {}}
        ]);
    });    
  };

  public render() {
    return (
      <View style={styles.form}>
        <View style={styles.formView}>
          <View style={styles.formTitleView}>
            <Text style={styles.formTitleText}>{`Welcome`}</Text>
            <TouchableOpacity onPress={this.signUp}>
              <Text style={styles.formSignUpText}>{`Sign Up`}</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.formContinueText}>{`Sign in to Continue`}</Text>
          {/* Email */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Email`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder=""
              onChangeText={(text) => this.setState({email: text})}
              value={this.state.email}
            />
          </View>
          {/* password */}
          <View style={styles.formItemInput}>
            <Text style={styles.formTitleInputText}>{`Password`}</Text>
            <TextInput
              style={styles.formInputText}
              placeholder="********"
              secureTextEntry={true}
              onChangeText={(text) => this.setState({password: text})}
              value={this.state.password}
            />
          </View>
          <TouchableOpacity onPress={this.forgot}>
            <Text
              style={styles.formForgetPasswordText}>{`Forgot Password?`}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.butonSignIn} onPress={this.signIn}>
            <Text style={styles.formButtonText}>{`SIGN IN`}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Form;
