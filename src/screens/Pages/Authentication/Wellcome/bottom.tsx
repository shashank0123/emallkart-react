import React from 'react';
import {Text, View, TouchableOpacity, ToastAndroid} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import styles from './styles';
import {rateWidth} from 'services/DeviceInfo';

class Body extends React.PureComponent<Props> {
  loginWithFacebook = () => {
    ToastAndroid.show('Login With facebook !', ToastAndroid.SHORT);
  };

  loginWithGoogle = () => {
    ToastAndroid.show('Login With Google!', ToastAndroid.SHORT);
  };
  public render() {
    return (
      <View style={styles.bottom}>
        <Text style={styles.bottomMoreText}>{`-OR-`}</Text>
        <TouchableOpacity
          style={styles.buttonOtherLogin}
          onPress={this.loginWithFacebook}>
          <Entypo color="#475993" name="facebook" size={25 * rateWidth} />
          <Text style={styles.bottomText}>{`Sign In with Facebook`}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonOtherLogin}
          onPress={this.loginWithGoogle}>
          <Entypo
            color="#ff3d00"
            name="google--with-circle"
            size={25 * rateWidth}
          />
          <Text style={styles.bottomText}>{`Sign In with Facebook`}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Body;
