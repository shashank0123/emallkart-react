import React from 'react';
import {View, BackHandler} from 'react-native';
import {connect} from 'react-redux';
import HeaderBar from 'screens/Molecules/Headerbar';
// Select Templete A example
import AuthenticationTemplate from 'screens/Templetes/Authentication';
import Body from './body';
import * as lauchActions from 'screens/Pages/Launch/actions';

interface Props {}

class Main extends React.Component<Props> {
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress,
    );
  }

  handleBackPress = () => {
    const {backButtonDevice} = this.props;

    backButtonDevice();
    BackHandler.exitApp();
    return true;
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  public render() {
    const headerComponent = <View />;
    const bodyComponent = <Body {...this.props} />;
    return (
      <AuthenticationTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {...lauchActions})(Main);
