import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import styles from './styles';
import FormView from './form';
import BottomView from './bottom';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.container}>
        <FormView {...this.props} />
        <BottomView />
      </View>
    );
  }
}

export default Body;
