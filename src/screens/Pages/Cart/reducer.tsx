import * as actionType from './actionTypes';
import {cartProducts} from 'datafake';

const INITIAL_STATE = {
  cartProducts: cartProducts,
  favouriteProducts: [],
  deviveryCase: 1,
  address: {
    isCheck: true,
    street1: '21, Alex Davidson Avenue',
    street2: 'Opposite Omegatron, Vicent Quarters',
    city: 'Victoria Island',
    state: 'Lagos State',
    country: 'Nigeria',
  },
  card: {},
};

const addProductToCart = (products, product) => {
  let isNew = true;
  const clone = {...product, amount: 1};
  products.forEach((item) => {
    if (item.id === product.id) {
      item.amount += 1;
      isNew = false;
    }
  });
  if (isNew) {
    return [...products, clone];
  }
  return products;
};

const decreaseProductToCart = (products, product) => {
  if (product.amount === 1) return deleteProductToCart(products, product);
  return products.map((item, index) => {
    const amount = item.id === product.id ? item.amount - 1 : item.amount;
    item.amount = amount;
    return item;
  });
};

const increaseProductToCart = (products, product) => {
  return products.map((item, index) => {
    const amount = item.id === product.id ? item.amount + 1 : item.amount;
    item.amount = amount;
    return item;
  });
};

const deleteProductToCart = (products, product) => {
  return products.filter((item, index) => item.id !== product.id);
};

const MainReducer = (state = INITIAL_STATE, action: any) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case actionType.ADD_PRODUCT_TO_CART:
      newState.cartProducts = [
        ...addProductToCart(newState.cartProducts, action.data),
      ];
      break;
    case actionType.DELETE_PRODUCT_FROM_CART:
      newState.cartProducts = deleteProductToCart(
        newState.cartProducts,
        action.data,
      );
      break;

    case actionType.INCREASE_PRODUCT_FROM_CART:
      newState.cartProducts = [
        ...increaseProductToCart(newState.cartProducts, action.data),
      ];
      break;

    case actionType.DECREASE_PRODUCT_FROM_CART:
      newState.cartProducts = [
        ...decreaseProductToCart(newState.cartProducts, action.data),
      ];
      break;

    case actionType.FAVOURITE_PRODUCT_FROM_CART:
      newState.favouriteProducts = [...newState.favouriteProducts, action.data];
      break;

    case actionType.SETTING_ADDRESS_PAYMENT:
      newState.address = Object.assign({}, action.data);
      break;

    case actionType.SETTING_CARD_PAYMENT:
      newState.card = Object.assign({}, action.data);
      break;

    case actionType.SETTING_DELIVERY_PAYMENT:
      newState.deviveryCase = action.data;
      break;
    default:
      return state;
  }
  return newState;
};

export default MainReducer;
