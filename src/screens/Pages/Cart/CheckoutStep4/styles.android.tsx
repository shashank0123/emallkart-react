import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginBottom: 100,
  },
  productView: {
    marginTop: 10 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    flexDirection: 'column',
  },

  line: {
    marginTop: 20 * rateHeight,
    marginHorizontal: 16 * rateWidth,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#dddddd',
  },

  // address
  addressView: {
    paddingTop: 20 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    justifyContent: 'center',
  },

  addressTitle: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  addressContentView: {
    marginTop: 20 * rateHeight,
    flexDirection: 'row',
  },

  addressText: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  changeText: {
    marginTop: 20 * rateHeight,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },
  // payment setting
  paymentView: {
    paddingTop: 20 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    justifyContent: 'center',
  },

  paymentTitle: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  cardView: {
    marginTop: 20 * rateHeight,
    height: 50 * rateHeight,
    flexDirection: 'row',
  },

  cardIcon: {
    width: 70 * rateWidth,
    height: 45 * rateHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor: '#00c569',
  },

  cardInfo: {
    paddingLeft: 16 * rateWidth,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  cardName: {
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#929292',
  },

  cardContent: {
    marginTop: 10 * rateHeight,
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 19,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },
});

export default styles;
