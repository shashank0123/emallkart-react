import React from 'react';

import styles from './styles';
import Form from './form';
import DialogLoading from 'screens/Molecules/DialogLoading';
import {ScrollView} from 'react-native-gesture-handler';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        <Form {...this.props} />
        <DialogLoading {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
