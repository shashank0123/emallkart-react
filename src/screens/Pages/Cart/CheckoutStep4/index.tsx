import React from 'react';
import {connect} from 'react-redux';
import HeaderBar from 'screens/Molecules/Headerbar';
// Select Templete A example
import * as Routes from 'navigator/routerName';
import ButtonCheckoutStep from 'screens/Atoms/ButtonCheckoutStep';
import CartTemplate from 'screens/Templetes/Cart';
import Body from './body';
import * as selectors from '../selectors';

interface Props {
  cartProducts: any;
  navigation: any;
}

class Main extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      paymentSucces: false,
    };
  }

  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  headerButtonAction = () => {
    // TODO
  };

  hideDialog = () => {
    const {navigation} = this.props;
    this.setState({loading: false}, () => {
      // TODO
      // delete cartProducts local and server
      navigation.navigate(Routes.EXPRORE_MAIN);
    });
  };

  nextAction = () => {
    // TODO
    this.setState({loading: true}, () => {
      setTimeout(() => {
        this.setState({paymentSucces: true});
      }, 3000);
    });
  };

  backAction = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.CART, {
      screen: Routes.CART_CHECKOUT_STEP3,
    });
  };
  public render() {
    const header = {
      label: 'Payment',
      backClick: this.backClick,
      isShowLeftButton: false,
      isShowLabel: true,
      actionClick: this.headerButtonAction,
      iconName: 'search1',
    };

    const bodyProps = {
      loading: this.state.loading,
      paymentSucces: this.state.paymentSucces,
      hideDialog: this.hideDialog,
    };
    const bottomProps = {
      isShowLeftButton: true,
      textLeftButton: 'BACK',
      textRightButton: 'PAY',
      actionLeftClick: this.backAction,
      actionRightClick: this.nextAction,
    };
    const headerComponent = <HeaderBar {...header} />;
    const bodyComponent = <Body {...this.props} {...bodyProps} />;
    const bottomComponent = <ButtonCheckoutStep {...bottomProps} />;
    return (
      <CartTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  cartProducts: selectors.getCartProducts(state),
});
export default connect(mapStateToProps, {})(Main);
