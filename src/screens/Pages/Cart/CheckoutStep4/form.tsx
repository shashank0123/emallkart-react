import React from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ProductCartPayItem from 'screens/Atoms/ProductCartPayItem';
import styles from './styles';
import * as actions from '../actions';
import * as selectors from '../selectors';
import * as exploreActions from 'screens/Pages/Explore/actions';

class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  selectProduct = (product: object) => {
    // TODO
    const {navigation} = this.props;
    const {chooseProductItem} = this.props;

    chooseProductItem(product);

    navigation.navigate(Routes.EXPRORE, {
      screen: Routes.EXPRORE_PRODUCT,
    });
  };

  changeCard = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.CART_CHECKOUT_STEP3);
  };
  changeAddress = () => {
    const {navigation} = this.props;
    navigation.navigate(Routes.CART_CHECKOUT_STEP2);
  };

  _onChange = (formData) => console.log(JSON.stringify(formData, null, ' '));

  getAddressDetail = () => {
    const {address} = this.props;
    return `${address.street1}, ${address.street2}, ${address.city}, ${address.state}, ${address.country}`;
  };

  getCardNumber = () => {
    const {card} = this.props;
    if (card.values) {
      const len = card.values.number.length;
      return card.values.number.substring(len - 5, len);
    }
    return '****';
  };

  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.productView}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.props.cartProducts}
            renderItem={({item: rowData}) => {
              return (
                <ProductCartPayItem
                  data={rowData}
                  actionClick={this.selectProduct}
                />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
        {/* line */}
        <View style={styles.line}></View>
        {/* address seting */}
        <View style={styles.addressView}>
          <Text style={styles.addressTitle}>{`Shipping Address`}</Text>
          <View style={styles.addressContentView}>
            <View style={{flex: 6}}>
              <Text style={styles.addressText}>{this.getAddressDetail()}</Text>
            </View>

            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign name="checkcircle" color={'#00c569'} size={20} />
              </View>
            </View>
          </View>
          <TouchableOpacity onPress={this.changeAddress}>
            <Text style={styles.changeText}>{`Change`}</Text>
          </TouchableOpacity>
        </View>
        {/* line */}
        <View style={styles.line}></View>
        {/* Cart */}
        <View style={styles.paymentView}>
          <Text style={styles.paymentTitle}>{`Payment`}</Text>
          <View style={styles.cardView}>
            <View style={{flex: 6, flexDirection: 'row'}}>
              <View style={styles.cardIcon}>
                <FontAwesome name="credit-card" color={'white'} size={30} />
              </View>
              <View style={styles.cardInfo}>
                <Text style={styles.cardName}>{`Master Card`}</Text>
                <Text
                  style={
                    styles.cardContent
                  }>{`**** **** **** ${this.getCardNumber()}`}</Text>
              </View>
            </View>

            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign name="checkcircle" color={'#00c569'} size={20} />
              </View>
            </View>
          </View>
          <TouchableOpacity onPress={this.changeCard}>
            <Text style={styles.changeText}>{`Change`}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  address: selectors.getAddressPayment(state),
  card: selectors.getCardPament(state),
});
export default connect(mapStateToProps, {...actions, ...exploreActions})(Form);
