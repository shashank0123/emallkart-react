import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Animated, TouchableOpacity, View, Text} from 'react-native';
import ProductCartItem from 'screens/Atoms/ProdudctCartItem';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {SwipeListView} from 'react-native-swipe-list-view';
import styles from './styles';
import {rateWidth} from 'services/DeviceInfo';

import * as actions from '../actions';
const rowSwipeAnimatedValues: any = {};

Array(50)
  .fill('')
  .forEach((_, i) => {
    rowSwipeAnimatedValues[`${i}`] = new Animated.Value(0);
  });

interface State {
  cartProducts: any;
}
class SwipeValueBasedUi extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  closeRow = (rowMap, rowKey) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  };

  favouriteRow = (rowMap, rowKey, data) => {
    this.closeRow(rowMap, rowKey);
    const {favouriteProductFromCart} = this.props;
    favouriteProductFromCart(data);
  };

  deleteRow = (rowMap, rowKey, data) => {
    this.closeRow(rowMap, rowKey);
    // Remove product from store and database
    // TODO

    // const deletedItem = cartProducts[prevIndex];
    const {deleteProductFromCart} = this.props;
    deleteProductFromCart(data);
  };

  onRowDidOpen = (rowKey) => {
    console.log('This row opened', rowKey);
  };

  onSwipeValueChange = (swipeData) => {
    const {key, value} = swipeData;
    rowSwipeAnimatedValues[key].setValue(Math.abs(value));
  };

  changeAmountProduct = (product, increase) => {
    const {increaseProductAmount, decreaseProductAmount} = this.props;
    if (increase) increaseProductAmount(product);
    else decreaseProductAmount(product);
    // TODO
  };

  renderItem = (data) => {
    return (
      <ProductCartItem
        data={data.item}
        changeAmount={this.changeAmountProduct}
      />
    );
  };

  renderHiddenItem = (data, rowMap) => {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          style={[styles.backLeftBtn, styles.backRightBtnLeft]}
          onPress={() => this.favouriteRow(rowMap, data.item.key, data.item)}>
          <AntDesign name="star" size={25} color="white" />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.backRightBtn, styles.backRightBtnRight]}
          onPress={() => this.deleteRow(rowMap, data.item.key, data.item)}>
          <Animated.View
            style={[
              styles.trash,
              {
                transform: [
                  {
                    scale: rowSwipeAnimatedValues[data.item.key].interpolate({
                      inputRange: [45, 90],
                      outputRange: [0, 1],
                      extrapolate: 'clamp',
                    }),
                  },
                ],
              },
            ]}>
            <AntDesign name="delete" size={25} color="white" />
          </Animated.View>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    if (this.props.cartProducts.length === 0)
      return (
        <View style={styles.blankView}>
          <Text style={styles.blankText}>{`No product on cart`}</Text>
        </View>
      );
    return (
      <View style={styles.container}>
        <SwipeListView
          data={this.props.cartProducts}
          renderItem={this.renderItem}
          renderHiddenItem={this.renderHiddenItem}
          leftOpenValue={75 * rateWidth}
          rightOpenValue={-75 * rateWidth}
          previewRowKey={'0'}
          previewOpenValue={-40}
          previewOpenDelay={3000}
          onRowDidOpen={this.onRowDidOpen}
          onSwipeValueChange={this.onSwipeValueChange}
        />
      </View>
    );
  }
}
/* @todo using :any */
const mapStateToProps = (state: any) => ({
  // cartProducts: selectors.getCartProducts(state),
});

export default connect(mapStateToProps, actions)(SwipeValueBasedUi);
