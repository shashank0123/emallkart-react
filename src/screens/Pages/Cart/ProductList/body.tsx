import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import styles from './styles';
import ProductList from './products';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.container}>
        <ProductList {...this.props} />
      </View>
    );
  }
}

export default Body;
