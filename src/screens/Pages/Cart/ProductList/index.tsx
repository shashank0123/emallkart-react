import React from 'react';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import ButtonCheckout from 'screens/Atoms/ButtonCheckout';
import Headerbar from 'screens/Molecules/HeaderCart';
import CartTemplate from 'screens/Templetes/Cart';
import Body from './body';
import * as selectors from '../selectors';

interface Props {
  cartProducts: any;
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  checkoutStep1 = () => {
    console.log('====>');

    const {navigation} = this.props;
    navigation.navigate(Routes.CART, {
      screen: Routes.CART_CHECKOUT_STEP1,
    });
  };

  assignCartProduct = () => {
    const {cartProducts} = this.props;
    return cartProducts.map((item, key) => {
      return {...item, key};
    });
  };
  public render() {
    const header = {
      actionClick: () => {},
      backClick: this.backClick,
      iconName: 'cart-arrow-down',
      isShowRightButton: true,
      isShowLabel: true,
      label: 'Cart',
    };
    const headerComponent = (
      <Headerbar {...header} data={this.props.cartProducts} />
    );
    const bodyComponent = <Body cartProducts={this.assignCartProduct()} />;
    const bottomComponent = (
      <ButtonCheckout
        data={this.props.cartProducts}
        actionClick={this.checkoutStep1}
      />
    );
    return (
      <CartTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  cartProducts: selectors.getCartProducts(state),
});

export default connect(mapStateToProps, {})(Main);
