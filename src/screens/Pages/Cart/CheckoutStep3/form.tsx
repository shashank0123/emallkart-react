import React from 'react';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from 'constants/colors';
import {CreditCardInput} from 'react-native-credit-card-input';
import * as actions from '../actions';
import styles from './styles';

interface State {
  isCheck: boolean;
}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      isCheck: true,
    };
  }

  _onChange = (form) => {
    const {settingCardPayment} = this.props;
    settingCardPayment({...form});
  };

  public render() {
    const {isCheck} = this.state;
    const checkBoxColor = isCheck ? '#00c569' : colors.backgroundDark;

    return (
      <View style={styles.form}>
        <View style={styles.graphicView}>
          <View style={styles.line}></View>
          <View style={styles.graphic}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Delivery`}</Text>
          </View>
          <View style={styles.graphic1}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Address`}</Text>
          </View>
          <View style={styles.graphic}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Payments`}</Text>
          </View>
        </View>
        <View style={styles.contentView}>
          <CreditCardInput onChange={this._onChange} />
        </View>
        {/* checkbox */}
        <View style={styles.view}>
          <TouchableOpacity
            style={styles.rowView}
            onPress={() => this.setState({isCheck: !isCheck})}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <AntDesign name="checkcircle" color={checkBoxColor} size={20} />
            </View>
            <View style={{flex: 9}}>
              <Text style={styles.contentText}>{`Save this card details`}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});
export default connect(mapStateToProps, {...actions})(Form);
