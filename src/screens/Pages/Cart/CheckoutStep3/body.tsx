import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import styles from './styles';
import FormView from './form';
import {ScrollView} from 'react-native-gesture-handler';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <ScrollView style={styles.container}>
        <FormView {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
