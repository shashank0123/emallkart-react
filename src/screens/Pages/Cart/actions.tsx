import * as actionTypes from './actionTypes';

// dialog
export const addProductToCart = (data: object) => {
  return {
    type: actionTypes.ADD_PRODUCT_TO_CART,
    data,
  };
};

export const deleteProductFromCart = (data: object) => {
  return {
    type: actionTypes.DELETE_PRODUCT_FROM_CART,
    data,
  };
};

export const favouriteProductFromCart = (data: object) => {
  return {
    type: actionTypes.FAVOURITE_PRODUCT_FROM_CART,
    data,
  };
};

export const increaseProductAmount = (data: object) => {
  return {
    type: actionTypes.INCREASE_PRODUCT_FROM_CART,
    data,
  };
};

export const decreaseProductAmount = (data: object) => {
  return {
    type: actionTypes.DECREASE_PRODUCT_FROM_CART,
    data,
  };
};

export const settingDeliveryPayment = (data: number) => {
  return {
    type: actionTypes.SETTING_DELIVERY_PAYMENT,
    data,
  };
};

export const settingAddressPayment = (data: object) => {
  return {
    type: actionTypes.SETTING_ADDRESS_PAYMENT,
    data,
  };
};

export const settingCardPayment = (data: object) => {
  return {
    type: actionTypes.SETTING_CARD_PAYMENT,
    data,
  };
};
