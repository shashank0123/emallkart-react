interface State {
  [KEY: string]: any;
}

export const getCartProducts = (state: State) =>
  state.screens.page.cart.cartProducts;
export const getFavouriteProducts = (state: State) =>
  state.screens.page.cart.favouriteProducts;

export const getDeliveryCase = (state: State) =>
  state.screens.page.cart.deviveryCase;

export const getCardPament = (state: State) => state.screens.page.cart.card;

export const getAddressPayment = (state: State) =>
  state.screens.page.cart.address;
