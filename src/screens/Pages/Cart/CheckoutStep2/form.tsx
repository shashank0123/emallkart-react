import React from 'react';
import {connect} from 'react-redux';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import * as Routes from 'navigator/routerName';
import * as selectors from '../selectors';
import * as actions from '../actions';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from 'constants/colors';
import styles from './styles';

interface State {
  isCheck: boolean;
  street1: string;
  street2: string;
  city: string;
  state: string;
  country: string;
}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  changeAddress = (data) => {
    const {address} = this.props;
    const {settingAddressPayment} = this.props;
    settingAddressPayment({...address, ...data});
  };

  public render() {
    const {isCheck} = this.props;
    const checkBoxColor = isCheck ? '#00c569' : colors.backgroundDark;

    return (
      <View style={styles.form}>
        <View style={styles.graphicView}>
          <View style={styles.line}></View>
          <View style={styles.graphic}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Delivery`}</Text>
          </View>
          <View style={styles.graphic1}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Address`}</Text>
          </View>
          <View style={styles.graphic}>
            <View style={styles.circleNormal1}>
              {/* <View style={styles.circle2}></View> */}
            </View>
            <Text style={styles.graphicTextNormal}>{`Payments`}</Text>
          </View>
        </View>
        <View style={styles.contentView}>
          {/* checkbox */}
          <View style={styles.view}>
            <TouchableOpacity
              style={styles.rowView}
              onPress={() => this.changeAddress({isCheck: !isCheck})}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign name="checkcircle" color={checkBoxColor} size={20} />
              </View>
              <View style={{flex: 9}}>
                <Text
                  style={
                    styles.contentText
                  }>{`Billing address is the same as delivery address`}</Text>
              </View>
            </TouchableOpacity>
          </View>
          {/* Address setting */}
          <View style={styles.inputView}>
            <TouchableOpacity style={styles.inputItemView}>
              <Text style={styles.inputTitleText}>{`Street 1`}</Text>
              <TextInput
                style={styles.inputContentText}
                onChangeText={(text) => this.changeAddress({street1: text})}
                value={this.props.street1}
              />
            </TouchableOpacity>

            <TouchableOpacity style={styles.inputItemView}>
              <Text style={styles.inputTitleText}>{`Street 2`}</Text>
              <TextInput
                style={styles.inputContentText}
                onChangeText={(text) => this.changeAddress({street2: text})}
                value={this.props.street2}
              />
            </TouchableOpacity>

            <TouchableOpacity style={styles.inputItemView}>
              <Text style={styles.inputTitleText}>{`City`}</Text>
              <TextInput
                style={styles.inputContentText}
                onChangeText={(text) => this.changeAddress({city: text})}
                value={this.props.city}
              />
            </TouchableOpacity>
            <View style={styles.inputItemRowView}>
              <TouchableOpacity style={styles.inputItemRowChild}>
                <Text style={styles.inputTitleText}>{`State`}</Text>
                <TextInput
                  style={styles.inputContentText}
                  onChangeText={(text) => this.changeAddress({state: text})}
                  value={this.props.state}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.inputItemRowChild}>
                <Text style={styles.inputTitleText}>{`Country`}</Text>
                <TextInput
                  style={styles.inputContentText}
                  onChangeText={(text) => this.changeAddress({country: text})}
                  value={this.props.country}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  address: selectors.getAddressPayment(state),
  isCheck: selectors.getAddressPayment(state)['isCheck'],
  street1: selectors.getAddressPayment(state)['street1'],
  street2: selectors.getAddressPayment(state)['street2'],
  city: selectors.getAddressPayment(state)['city'],
  state: selectors.getAddressPayment(state)['state'],
  country: selectors.getAddressPayment(state)['country'],
});
export default connect(mapStateToProps, {...actions})(Form);
