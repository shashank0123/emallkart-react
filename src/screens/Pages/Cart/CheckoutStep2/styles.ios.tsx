import {StyleSheet} from 'react-native';
import {
  rateHeight,
  rateWidth,
  deviceWidth,
  isDeviceIphoneX,
} from 'services/DeviceInfo';
import IphoneX from './styles.ios.x';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },

  form: {
    flex: 1,
    justifyContent: 'center',
  },

  graphicView: {
    // flex: 1,
    height: 100 * rateHeight,
    paddingTop: 20,
    flexDirection: 'row',
    // backgroundColor: 'red',
  },

  contentView: {
    marginBottom: 100,
  },

  // graphicView
  graphic: {
    flex: 1,
    alignItems: 'center',
  },
  graphic1: {
    flex: 3,
    alignItems: 'center',
  },
  circle2: {
    width: 16,
    height: 16,
    backgroundColor: '#00c569',
    borderRadius: 30,
  },

  circle1: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#00c569',
  },

  circleNormal1: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },

  graphicTextBold: {
    marginTop: 14,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#000000',
  },

  graphicTextNormal: {
    marginTop: 14,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.5)',
  },

  line: {
    width: 300,
    position: 'absolute',
    marginTop: 35,
    marginHorizontal: 20,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#dddddd',
  },

  //  ContentView
  view: {
    paddingHorizontal: 16 * rateWidth,
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  viewCheck: {
    width: 24,
    height: 24,
    borderRadius: 30,
    backgroundColor: '#00c569',
  },
  viewUncheck: {},
  titleText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },
  contentText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 23,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  // Address content
  inputView: {
    paddingHorizontal: 16 * rateWidth,
  },
  inputItemView: {
    marginTop: 20 * rateHeight,
    height: 67 * rateHeight,
  },
  inputItemRowView: {
    marginTop: 20 * rateHeight,
    flexDirection: 'row',
    height: 67 * rateHeight,
  },
  inputItemRowChild: {
    flex: 1,
    height: 67 * rateHeight,
  },
  inputTitleText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  inputContentText: {
    fontFamily: 'System',
    marginTop: 10 * rateHeight,
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
