import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },

  form: {
    flex: 1,
    justifyContent: 'center',
  },

  graphicView: {
    flex: 1,
    paddingTop: 20,
    flexDirection: 'row',
    // backgroundColor: 'red',
  },

  contentView: {
    flex: 3,
    marginBottom: 100,
  },

  // graphicView
  graphic: {
    flex: 1,
    alignItems: 'center',
  },
  graphic1: {
    flex: 3,
    alignItems: 'center',
  },
  circle2: {
    width: 16,
    height: 16,
    backgroundColor: '#00c569',
    borderRadius: 30,
  },

  circle1: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#00c569',
  },

  circleNormal1: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },

  graphicTextBold: {
    marginTop: 14,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#000000',
  },

  graphicTextNormal: {
    marginTop: 14,
    fontFamily: 'System',
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.5)',
  },

  line: {
    width: 300,
    position: 'absolute',
    marginTop: 35,
    marginHorizontal: 20,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#dddddd',
  },

  //  ContentView
  view: {
    flex: 1,
    paddingHorizontal: 16 * rateWidth,
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },
  contentText: {
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 23,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },
});

export default styles;
