import React from 'react';
import {connect} from 'react-redux';
import HeaderBar from 'screens/Molecules/Headerbar';
// Select Templete A example
import CartTemplate from 'screens/Templetes/Cart';
import ButtonCheckoutStep from 'screens/Atoms/ButtonCheckoutStep';
import * as Routes from 'navigator/routerName';
import Body from './body';
import * as selectors from '../selectors';

interface Props {
  cartProducts: any;
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  headerButtonAction = () => {
    // TODO
  };

  nextAction = () => {
    const {navigation} = this.props;

    navigation.navigate(Routes.CART, {
      screen: Routes.CART_CHECKOUT_STEP2,
    });
  };

  public render() {
    const header = {
      label: 'Delivery',
      backClick: this.backClick,
      isShowLeftButton: false,
      isShowLabel: true,
      actionClick: this.headerButtonAction,
      iconName: 'search1',
    };

    const bottomProps = {
      isShowLeftButton: false,
      textLeftButton: 'BACK',
      textRightButton: 'NEXT',
      actionLeftClick: () => {},
      actionRightClick: this.nextAction,
    };
    const headerComponent = <HeaderBar {...header} />;
    const bodyComponent = <Body {...this.props} />;
    const bottomComponent = <ButtonCheckoutStep {...bottomProps} />;
    return (
      <CartTemplate
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
        bottomComponent={bottomComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  cartProducts: selectors.getCartProducts(state),
});
export default connect(mapStateToProps, {})(Main);
