import React from 'react';
import {connect} from 'react-redux';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import {CheckBox} from 'react-native-elements';

import styles from './styles';
import * as selectors from '../selectors';
import * as actions from '../actions';

interface State {}
class Form extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
  }

  public render() {
    console.log(this.props);
    return (
      <View style={styles.form}>
        <View style={styles.graphicView}>
          <View style={styles.line}></View>
          <View style={styles.graphic}>
            <View style={styles.circle1}>
              <View style={styles.circle2}></View>
            </View>
            <Text style={styles.graphicTextBold}>{`Delivery`}</Text>
          </View>
          <View style={styles.graphic1}>
            <View style={styles.circleNormal1}>
              {/* <View style={styles.circle2}></View> */}
            </View>
            <Text style={styles.graphicTextNormal}>{`Address`}</Text>
          </View>
          <View style={styles.graphic}>
            <View style={styles.circleNormal1}>
              {/* <View style={styles.circle2}></View> */}
            </View>
            <Text style={styles.graphicTextNormal}>{`Payments`}</Text>
          </View>
        </View>
        <View style={styles.contentView}>
          {/* case1 */}
          <View style={styles.view}>
            <Text style={styles.titleText}>{`Standard Delivery`}</Text>
            <View style={styles.rowView}>
              <View style={{flex: 5}}>
                <Text
                  style={
                    styles.contentText
                  }>{`Order will be delivered between 3 - 5 business days`}</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}>
                <CheckBox
                  checkedColor="#00c569"
                  iconRight
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={this.props.deliveryCase === 1}
                  onPress={() => this.props.settingDeliveryPayment(1)}
                />
              </View>
            </View>
          </View>
          {/* case2 */}
          <View style={styles.view}>
            <Text style={styles.titleText}>{`Next Day Delivery`}</Text>
            <View style={styles.rowView}>
              <View style={{flex: 5}}>
                <Text
                  style={
                    styles.contentText
                  }>{`Place your order before 6pm and your items will be delivered the next day`}</Text>
              </View>

              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}>
                <CheckBox
                  checkedColor="#00c569"
                  iconRight
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={this.props.deliveryCase === 2}
                  onPress={() => this.props.settingDeliveryPayment(2)}
                />
              </View>
            </View>
          </View>
          {/* case3 */}
          <View style={styles.view}>
            <Text style={styles.titleText}>{`Nominated Delivery`}</Text>
            <View style={styles.rowView}>
              <View style={{flex: 5}}>
                <Text
                  style={
                    styles.contentText
                  }>{`Pick a particular date from the calendar and order will be delivered on selected date`}</Text>
              </View>

              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}>
                <CheckBox
                  checkedColor="#00c569"
                  iconRight
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checked={this.props.deliveryCase === 3}
                  onPress={() => this.props.settingDeliveryPayment(3)}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  deliveryCase: selectors.getDeliveryCase(state),
});
export default connect(mapStateToProps, {...actions})(Form);
