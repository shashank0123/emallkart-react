import {StyleSheet} from 'react-native';
import colors from 'constants/colors';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },

  addressView: {
    paddingTop: 20 * rateHeight,
    paddingHorizontal: 16 * rateWidth,
    justifyContent: 'center',
  },

  addressTitle: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  addressContentView: {
    marginTop: 20 * rateHeight,
    flexDirection: 'row',
  },

  addressText: {
    fontFamily: 'System',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 26,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  changeText: {
    marginTop: 20 * rateHeight,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    textDecorationLine: 'underline',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00c569',
  },
});

export default styles;
