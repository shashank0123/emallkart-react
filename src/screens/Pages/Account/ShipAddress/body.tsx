import React, {Component} from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';

export default class Example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addressSetting: 1,
    };
  }
  render() {
    const notSelectColor = 'rgba(0, 0, 0, 0.06)';
    const selectedColor = '#00c569';
    const {addressSetting} = this.state;
    return (
      <View style={styles.bodyContent}>
        <View style={styles.addressView}>
          <Text style={styles.addressTitle}>{`Home Address`}</Text>
          <TouchableOpacity
            style={styles.addressContentView}
            onPress={() => this.setState({addressSetting: 1})}>
            <View style={{flex: 6}}>
              <Text
                style={
                  styles.addressText
                }>{`21, Alex Davidson Avenue, Opposite Omegatron, Vicent Smith Quarters, Victoria Island, Lagos, Nigeria`}</Text>
            </View>

            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign
                  name="checkcircle"
                  color={
                    this.state.addressSetting === 1
                      ? selectedColor
                      : notSelectColor
                  }
                  size={20}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>

        {/* word address */}

        <View style={styles.addressView}>
          <Text style={styles.addressTitle}>{`Work Address`}</Text>
          <TouchableOpacity
            style={styles.addressContentView}
            onPress={() => this.setState({addressSetting: 2})}>
            <View style={{flex: 6}}>
              <Text
                style={
                  styles.addressText
                }>{`21, Alex Davidson Avenue, Opposite Omegatron, Vicent Smith Quarters, Victoria Island, Lagos, Nigeria`}</Text>
            </View>

            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign
                  name="checkcircle"
                  color={
                    this.state.addressSetting === 2
                      ? selectedColor
                      : notSelectColor
                  }
                  size={20}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
        {/*  */}
        <View style={styles.addressView}>
          <Text style={styles.addressTitle}>{`Other Address`}</Text>
          <TouchableOpacity
            style={styles.addressContentView}
            onPress={() => this.setState({addressSetting: 3})}>
            <View style={{flex: 6}}>
              <Text
                style={
                  styles.addressText
                }>{`21, Alex Davidson Avenue, Opposite Omegatron, Vicent Smith Quarters, Victoria Island, Lagos, Nigeria`}</Text>
            </View>

            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <AntDesign
                  name="checkcircle"
                  color={
                    this.state.addressSetting === 3
                      ? selectedColor
                      : notSelectColor
                  }
                  size={20}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
