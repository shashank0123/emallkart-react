import {StyleSheet} from 'react-native';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },

  bodyContent: {
    flex: 1,
    padding: 20,
  },

  list: {
    flex: 1,
    marginTop: 20,
  },
});

export default styles;
