import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import styles from './styles';
export default class Example extends Component {
  constructor() {
    super();
    this.data = [
      {
        time: '20/18\n09:00 AM',
        title: 'Order Signed',
        description: 'Lagos State, Nigeria',
        circleColor: '#00c569',
        lineColor: '#00c569',
      },
      {
        time: '20/18\n09:30 AM',
        circleColor: '#00c569',
        lineColor: '#00c569',
        title: 'Order Processed',
        description: 'Lagos State, Nigeria',
      },
      {
        time: '20/18\n10:00 AM',
        title: 'Shipped ',
        circleColor: '#00c569',
        lineColor: '#F0F0F0',
        description: 'Lagos State, Nigeria',
      },
      {
        time: '20/18\n10:10 AM',
        title: 'Out for delivery',
        description: 'Edo State, Nigeria',
        lineColor: '#F0F0F0',
        circleColor: '#F0F0F0',
      },
      {
        time: '20/18\n10:20 AM',
        title: 'Delivered',
        description: 'Edo State, Nigeria',
        lineColor: '#F0F0F0',
        circleColor: '#F0F0F0',
      },
    ];
  }

  render() {
    return (
      <View style={styles.bodyContent}>
        <Timeline
          style={styles.list}
          data={this.data}
          separator={true}
          circleSize={20}
          circleColor="rgb(45,156,219)"
          lineColor="rgb(45,156,219)"
          timeContainerStyle={{minWidth: 52, marginTop: 0}}
          timeStyle={{
            textAlign: 'center',
            backgroundColor: '#ff9797',
            color: 'white',
            padding: 5,
            borderRadius: 13,
            overflow: 'hidden',
          }}
          descriptionStyle={{color: 'gray'}}
          options={{
            style: {paddingTop: 5},
          }}
        />
      </View>
    );
  }
}
