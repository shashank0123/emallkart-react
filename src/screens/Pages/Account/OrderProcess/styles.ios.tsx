import {StyleSheet} from 'react-native';
import {isDeviceIphoneX, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';
import IphoneX from './styles.ios.x';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },

  bodyContent: {
    flex: 1,
    padding: 20,
  },

  list: {
    flex: 1,
    marginTop: 20,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
