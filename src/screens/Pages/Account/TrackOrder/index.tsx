import React from 'react';
import {connect} from 'react-redux';
import {View, StatusBar, ScrollView} from 'react-native';
import HeaderBar from 'screens/Molecules/Headerbar';
// Select Templete A example
import AccountItem from 'screens/Templetes/AccountItem';
import * as Routes from 'navigator/routerName';
import Body from './body';
import * as selectors from '../selectors';
import * as actions from '../actions';

interface Props {
  cartProducts: any;
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  headerButtonAction = () => {
    // TODO
  };

  nextAction = () => {
    const {navigation} = this.props;

    navigation.navigate(Routes.CART, {
      screen: Routes.CART_CHECKOUT_STEP2,
    });
  };

  public render() {
    const header = {
      label: 'Track Order',
      backClick: this.backClick,
      isShowLeftButton: false,
      isShowLabel: true,
      actionClick: this.headerButtonAction,
      iconName: 'search1',
    };

    const headerComponent = <HeaderBar {...header} />;
    const bodyComponent = <Body {...this.props} />;
    return (
      <AccountItem
        headerComponent={headerComponent}
        bodyComponent={bodyComponent}
      />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  orderTrack: selectors.getOrderTrack(state),
});
export default connect(mapStateToProps, {...actions})(Main);
