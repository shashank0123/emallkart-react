import React from 'react';
import {Text, View, FlatList} from 'react-native';
import OrderItem from 'screens/Atoms/OrderItem';
import * as Routes from 'navigator/routerName';
import {orders} from 'datafake';
import styles from './styles';

class Body extends React.PureComponent<Props> {
  showOrderStatusProcess = (data) => {
    const {navigation} = this.props;
    const {getOrderTrack} = this.props;

    getOrderTrack(data);

    navigation.navigate(Routes.ACCOUNT_MENU, {
      screen: Routes.ACCOUNT_TRACK_ORDER_PROCESS,
    });
  };
  public render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={orders}
          renderItem={({item: rowData}) => {
            return (
              <OrderItem
                data={rowData}
                actionClick={this.showOrderStatusProcess}
              />
            );
          }}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

export default Body;
