import {StyleSheet} from 'react-native';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },
});

export default styles;
