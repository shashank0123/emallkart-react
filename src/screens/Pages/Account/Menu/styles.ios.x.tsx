import {StyleSheet} from 'react-native';
import {rateWidth, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16 * rateWidth,
    flexDirection: 'column',
  },

  avatarContentView: {
    marginTop: 74 * rateHeight,
    flexDirection: 'row',
  },

  avatarView: {
    // flex: 1,
    height: 120 * rateHeight,
  },

  nameView: {
    flex: 2,
    paddingLeft: 30 * rateWidth,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  avatar: {
    width: 120 * rateWidth,
    height: 120 * rateWidth,
    borderRadius: 60,
  },

  nameText: {
    fontFamily: 'System',
    fontSize: 26,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 31,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  emailText: {
    marginTop: 9,
    fontFamily: 'System',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },

  // menu item
  menuView: {
    marginTop: 20 * rateHeight,
    flexDirection: 'row',
  },

  menuIcon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor: 'rgba(0, 197, 105, 0.07)',
  },
  menuTitle: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15,
  },
  menuButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  titleText: {
    fontFamily: 'System',
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 21,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#000000',
  },
});

export default styles;
