import React from 'react';
import * as Routes from 'navigator/routerName';
import {Text, View, TouchableOpacity, ToastAndroid} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';

class Body extends React.PureComponent<Props> {
  editProfile = () => {
    // TODO
    ToastAndroid.show('Edit profile.', ToastAndroid.SHORT);
  };

  showShippingAdd = () => {
    // TODO
    ToastAndroid.show('showShippingAdd.', ToastAndroid.SHORT);
    const {navigation} = this.props;

    navigation.navigate(Routes.ACCOUNT_MENU, {
      screen: Routes.ACCOUNT_SHIP_ADDRESS,
    });
  };

  trackOrder = () => {
    const {navigation} = this.props;

    navigation.navigate(Routes.ACCOUNT_MENU, {
      screen: Routes.ACCOUNT_TRACK_ORDER,
    });
  };
  public render() {
    return (
      <View>
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <AntDesign name="edit" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Edit Profile`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/* Shipping address */}
        <TouchableOpacity
          style={styles.menuView}
          onPress={this.showShippingAdd}>
          <View style={styles.menuIcon}>
            <FontAwesome name="map-marker" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Shipping Address`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/*  Wishlist*/}
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <AntDesign name="hearto" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Wishlist`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/* Order History */}
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <AntDesign name="hearto" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Order History`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuView} onPress={this.trackOrder}>
          <View style={styles.menuIcon}>
            <MaterialCommunityIcons
              name="tractor"
              color={'#111111'}
              size={25}
            />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Track Order`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/* Cards */}
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <AntDesign name="idcard" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Cards`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/* Notifications */}
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <MaterialIcons
              name="notifications-active"
              color={'#111111'}
              size={25}
            />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Notifications`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
        {/* Log Out */}
        <TouchableOpacity style={styles.menuView} onPress={this.editProfile}>
          <View style={styles.menuIcon}>
            <AntDesign name="logout" color={'#111111'} size={25} />
          </View>
          <View style={styles.menuTitle}>
            <Text style={styles.titleText}>{`Log Out`}</Text>
          </View>
          <View style={styles.menuButton}>
            <FontAwesome name="angle-right" color={'#000000'} size={25} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Body;
