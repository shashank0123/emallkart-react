import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import * as Routes from 'navigator/routerName';
import Avatar from './avatar';
import Menu from './menu';
import styles from './styles';
import {ScrollView} from 'react-native-gesture-handler';

class Body extends React.PureComponent<Props> {
  onclick = () => {
    const {navigation} = this.props;

    navigation.navigate(Routes.ACCOUNT_MENU, {
      screen: Routes.ACCOUNT_TRACK_ORDER,
    });
  };
  public render() {
    return (
      <ScrollView
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}>
        <Avatar />
        <Menu {...this.props} />
      </ScrollView>
    );
  }
}

export default Body;
