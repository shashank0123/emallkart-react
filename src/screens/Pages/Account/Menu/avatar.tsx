import React from 'react';
import {Text, View, Image} from 'react-native';
import {profile} from 'datafake';
import styles from './styles';

class Body extends React.PureComponent<Props> {
  public render() {
    return (
      <View style={styles.avatarContentView}>
        <View style={styles.avatarView}>
          <Image source={profile.image} style={styles.avatar} />
        </View>
        <View style={styles.nameView}>
          <Text style={styles.nameText}>{profile.user_name}</Text>
          <Text style={styles.emailText}>{profile.email}</Text>
        </View>
      </View>
    );
  }
}

export default Body;
