import React from 'react';
import {connect} from 'react-redux';
// Select Templete A example
import AccountMenu from 'screens/Templetes/AccountMenu';
import Body from './body';

interface Props {
  navigation: any;
}

class Main extends React.Component<Props> {
  backClick = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  public render() {
    const bodyComponent = <Body {...this.props} />;
    return <AccountMenu bodyComponent={bodyComponent} />;
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({});

export default connect(mapStateToProps, {})(Main);
