import * as actionTypes from './actionTypes';

// dialog
export const getOrderTrack = (data: object) => {
  return {
    type: actionTypes.SELECT_ORDER_HISTORY,
    data,
  };
};
