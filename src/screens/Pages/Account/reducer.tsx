import * as actionType from './actionTypes';

const INITIAL_STATE = {
  orderTrack: {},
};

const MainReducer = (state = INITIAL_STATE, action: any) => {
  const newState = Object.assign({}, state);
  switch (action.type) {
    case actionType.SELECT_ORDER_HISTORY:
      newState.orderTrack = action.data;
      break;

    default:
      return state;
  }
  return newState;
};

export default MainReducer;
