interface State {
  [KEY: string]: any;
}

export const getOrderTrack = (state: State) =>
  state.screens.page.account.orderTrack;
