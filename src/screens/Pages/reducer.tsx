import {combineReducers} from 'redux';
import ExploreReducer from './Explore/reducer';
import CartReducer from './Cart/reducer';
import LaunchReducer from './Launch/reducer';
import AccountReducer from './Account/reducer';

const Reducer = combineReducers({
  explore: ExploreReducer,
  cart: CartReducer,
  account: AccountReducer,
  launch: LaunchReducer,
});

export default Reducer;
