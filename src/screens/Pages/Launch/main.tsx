import React from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import * as Routes from 'navigator/routerName';
import * as actions from './actions';
import * as selectors from './selectors';
import styles from './styles';

interface Props {
  isAppReady: boolean;
  navigation: any;
  backButtonDevice: () => void;
  initialize: () => void;
}

class LaunchScreen extends React.Component<Props> {
  public componentDidMount() {
    const {initialize} = this.props;
    initialize();
  }

  public componentDidUpdate() {
    const {isAppReady, navigation} = this.props;
    if (isAppReady) {
      setTimeout(() => {
        navigation.navigate(Routes.AUTHENTICATION, {
          screen: Routes.AUTH_WELLCOME,
          initial: false,
        });
      }, 1000);
      // navigation.navigate(Routes.AUTHENTICATION, {
      //   screen: Routes.AUTH_WELLCOME,
      //   initial: false,
      // });
    } else {
      // TODO
      navigation.navigate(Routes.AUTHENTICATION, {
        screen: Routes.AUTH_WELLCOME,
        initial: false,
      });
    }
  }

  public handleBackButtonClick = () => {
    const {navigation, backButtonDevice} = this.props;
    const isGoBack = navigation.goBack();
    if (!isGoBack) {
      backButtonDevice();
    }
    return isGoBack;
  };

  public render() {
    return (
      <View style={styles.containerStyle}>
        <View style={styles.welcomeViewStyle}>
          <Text style={styles.titleStyle}>{`Loading...`}</Text>
        </View>
      </View>
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  isAppReady: selectors.getIsAppReady(state),
});

export default connect(mapStateToProps, {...actions})(LaunchScreen);
