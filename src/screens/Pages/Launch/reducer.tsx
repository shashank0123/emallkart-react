import {combineReducers} from 'redux';
import * as actionType from './actionType';

const INITIAL_STATE = {
  loading: false,
  isAppReady: false,
  errorMessage: '',
};

const MainReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case actionType.INITIALIZE_FINISHED:
      return {
        ...state,
        isAppReady: action.isAppReady,
      };
    default:
      return state;
  }
};

export default combineReducers({
  main: MainReducer,
});
