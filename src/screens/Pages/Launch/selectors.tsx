interface State {
  [Key: string]: any;
}
export const getIsAppReady = (state: State) =>
  state.screens.page.launch.main.isAppReady;
export const getMessageError = (state: State) =>
  state.screens.page.launch.main.errorMessage;
