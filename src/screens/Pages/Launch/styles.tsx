import {StyleSheet} from 'react-native';
import fonts from 'constants/fonts';
import {rateHeight, rateWidth} from 'services/DeviceInfo';

const margin = 0;
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    margin,
  },

  welcomeViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageStyle: {
    height: 150 * rateHeight,
    width: 150 * rateWidth,
  },

  titleStyle: {
    fontFamily: fonts.regular,
    color: '#008E57',
    fontSize: 24,
    margin,
  },
});

export default styles;
