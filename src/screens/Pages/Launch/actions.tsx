import * as actionType from './actionType';

export const initialize = () => {
  return {
    type: actionType.INITIALIZE_DOING,
    isAppReady: false,
  };
};

export const initializeFinished = () => {
  // TODO: init config
  return {
    type: actionType.INITIALIZE_FINISHED,
    isAppReady: true,
  };
};

export const backButtonDevice = () => {
  return {
    type: actionType.PROCESS_BACK_BUTTON_DEVICES,
  };
};
