import {takeLatest, put, select, call, take} from 'redux-saga/effects';

import {INITIALIZE_DOING} from './actionType';
import {initializeFinished} from './actions';

export function* initializeApp() {
  try {
    yield put(initializeFinished());
  } catch (error) {
    console.log('initializeApp', error);
  }
}

export default function* rootSaga() {
  yield takeLatest(INITIALIZE_DOING, initializeApp);
}
