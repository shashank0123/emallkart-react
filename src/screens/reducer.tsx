import {combineReducers} from 'redux';
import PageReducer from './Pages/reducer';

const Reducer = combineReducers({
  page: PageReducer,
});

export default Reducer;
