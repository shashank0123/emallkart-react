import {fork} from 'redux-saga/effects';
import LaunchSaga from './Pages/Launch/saga';

export default function* rootSaga() {
  yield fork(LaunchSaga);
}
