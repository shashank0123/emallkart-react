import {Platform} from 'react-native';
import ComponentIOS from './styles.ios';
import ComponentAndroid from './styles.android';

export default Platform.select({
  ios: () => ComponentIOS,
  android: () => ComponentAndroid,
})();
