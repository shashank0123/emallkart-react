import React from 'react';
import {View, StatusBar} from 'react-native';
import styles from './styles';

interface Props {
  headerComponent: React.ComponentElement;
  bodyComponent: React.ComponentElement;
}

class Main extends React.Component<Props> {
  public render() {
    const {headerComponent, bodyComponent} = this.props;
    const {container, header, body} = styles;
    return (
      <View style={container}>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          barStyle="dark-content"
        />
        <View style={header}>{headerComponent}</View>
        <View style={body}>{bodyComponent}</View>
      </View>
    );
  }
}

export default Main;
