import {StyleSheet} from 'react-native';
import colors from 'constants/colors';
import {rateHeight, deviceWidth, deviceHeight} from 'services/DeviceInfo';

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  header: {
    zIndex: 1,
    flex: 0.8,
  },

  body: {
    zIndex: 0,
    flex: 5,
    // backgroundColor: colors.backgroundDark,
  },

  bottom: {
    zIndex: 1,
    position: 'absolute',
    height: 120 * rateHeight,
    width: deviceWidth,
    // Sub tabbar height
    marginTop: deviceHeight - (60 + 50) * rateHeight,
  },
});
