import {StyleSheet} from 'react-native';
import {rateHeight, deviceWidth, deviceHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f1f6',
  },

  header: {
    // flex: 1,
    height: 65 * rateHeight,
    backgroundColor: colors.mainGreen,
  },

  body: {
    // flex: 9,
    // height: 500 * rateHeight,
    backgroundColor: colors.backgroundDark,
    paddingBottom: 60 * rateHeight,
  },
  bottom: {
    position: 'absolute',
    height: 60 * rateHeight,
    width: deviceWidth,
    marginTop: deviceHeight - 60 * rateHeight,
  },
});
