import {StyleSheet} from 'react-native';
import {
  isDeviceIphoneX,
  rateHeight,
  deviceWidth,
  deviceHeight,
} from 'services/DeviceInfo';
import colors from 'constants/colors';
import IphoneX from './styles.ios.x';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f1f6',
  },

  header: {
    // flex: 1,
    height: 58 * rateHeight,
    backgroundColor: colors.mainGreen,
  },

  body: {
    // flex: 9,
    // height: 500 * rateHeight,
    backgroundColor: colors.backgroundDark,
    paddingBottom: 60 * rateHeight,
  },
  bottom: {
    position: 'absolute',
    height: 60 * rateHeight,
    width: deviceWidth,
    marginTop: deviceHeight - 60 * rateHeight,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
