import {StyleSheet} from 'react-native';
import colors from 'constants/colors';
import {rateHeight, deviceWidth, deviceHeight} from 'services/DeviceInfo';

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  header: {
    position: 'absolute',
    zIndex: 1,
    height: 130 * rateHeight,
  },

  body: {
    zIndex: 0,
    flex: 5,
    // backgroundColor: colors.backgroundDark,
  },

  bottom: {
    position: 'absolute',
    height: 60 * rateHeight,
    width: deviceWidth,
    marginTop: deviceHeight - 60 * rateHeight,
  },
});
