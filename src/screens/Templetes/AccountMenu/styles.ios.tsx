import {StyleSheet} from 'react-native';
import {isDeviceIphoneX, rateHeight} from 'services/DeviceInfo';
import colors from 'constants/colors';
import IphoneX from './styles.ios.x';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },

  header: {
    zIndex: 1,
    flex: 1,
  },

  body: {
    zIndex: 0,
    flex: 5,
    backgroundColor: colors.backgroundDark,
  },
});

const isIphoneX = isDeviceIphoneX() ? IphoneX : styles;
export default isIphoneX;
