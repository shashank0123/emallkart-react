import {StyleSheet} from 'react-native';
import colors from 'constants/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: colors.backgroundDark,
  },

  header: {
    zIndex: 1,
    flex: 1,
  },

  body: {
    zIndex: 0,
    flex: 5,
    backgroundColor: colors.backgroundDark,
  },
});
