import React from 'react';
import {View, StatusBar, ScrollView} from 'react-native';
import styles from './styles';

interface Props {
  bodyComponent: React.ComponentElement;
}

class Main extends React.Component<Props> {
  public render() {
    const {bodyComponent} = this.props;
    const {container, body} = styles;
    return (
      <View style={container}>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          barStyle="dark-content"
        />
        <View style={body}>{bodyComponent}</View>
      </View>
    );
  }
}

export default Main;
