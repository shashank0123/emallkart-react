import {StyleSheet} from 'react-native';
import colors from 'constants/colors';
import {rateHeight, deviceWidth, deviceHeight} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  header: {
    zIndex: 1,
    height: 100 * rateHeight,
  },

  body: {
    zIndex: 0,
    flex: 5,
    // backgroundColor: colors.backgroundDark,
  },

  bottom: {
    zIndex: 1,
    position: 'absolute',
    backgroundColor: colors.backgroundDark,
    height: 100 * rateHeight,
    width: deviceWidth,
    marginTop: deviceHeight - 100 * rateHeight,
  },
});

export default styles;
