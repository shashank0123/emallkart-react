import React from 'react';
import {View, StatusBar, ScrollView} from 'react-native';
import styles from './styles';

interface Props {
  headerComponent: React.ComponentElement;
  bodyComponent: React.ComponentElement;
  bottomComponent: React.ComponentElement;
}

class Main extends React.Component<Props> {
  public render() {
    const {headerComponent, bodyComponent, bottomComponent} = this.props;
    const {container, header, body, bottom} = styles;
    return (
      <View style={container}>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          barStyle="dark-content"
        />
        <View style={header}>{headerComponent}</View>
        <View style={body}>{bodyComponent}</View>
        <View style={[bottom, {backgroundColor: '#fff'}]}>
          {bottomComponent}
        </View>
      </View>
    );
  }
}

export default Main;
