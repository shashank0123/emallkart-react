import {StyleSheet} from 'react-native';
import {rateHeight, deviceWidth, deviceHeight} from 'services/DeviceInfo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 0,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  header: {
    zIndex: 1,
    height: 130 * rateHeight,
  },

  body: {
    zIndex: 0,
    flex: 1,
  },

  bottom: {
    position: 'absolute',
    height: 60 * rateHeight,
    width: deviceWidth,
    marginTop: deviceHeight - 60 * rateHeight,
  },
});

export default styles;
