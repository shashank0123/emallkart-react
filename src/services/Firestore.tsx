import firebase from 'react-native-firebase'
/* tslint:disable */
const firestore = firebase.firestore()
const { settings } = firestore
if (settings) {
  settings.areTimestampsInSnapshotsEnabled = true
  settings.isPersistenceEnabled = true
}
firestore.settings = settings
firestore.FacebookAuthProvider = firebase.auth.FacebookAuthProvider
firestore.TwitterAuthProvider = firebase.auth.TwitterAuthProvider
firestore.GoogleAuthProvider = firebase.auth.GoogleAuthProvider
export default firestore