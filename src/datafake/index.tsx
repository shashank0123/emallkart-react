const image1 = require('assets/images/products/image1.png');
const image2 = require('assets/images/products/image2.png');
const image3 = require('assets/images/products/image3.png');
const image4 = require('assets/images/products/image4.png');
const image5 = require('assets/images/products/image5.png');
const image6 = require('assets/images/products/image6.png');

const avatar1 = require('assets/images/avatar/avatar1.png');
const avatar2 = require('assets/images/avatar/avatar2.png');
export const orders = [
  {
    created_at: Date.now(),
    products: [
      {
        image: image1,
        product_name: 'BeoPlay Speaker',
        id: 1,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2300,
      },
      {
        image: image2,
        product_name: 'Smart BeoPlay Speaker',
        id: 2,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 1200,
      },
      {
        image: image3,
        product_name: 'Smart Bluetooth Speaker',
        id: 3,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 2000,
      },
      {
        image: image4,
        product_name: 'Bluetooth Speaker',
        id: 4,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2500,
      },
    ],
    order_name: 'OD - 424923192 - N',
    price: 4500,
    status: 'In Transit',
  },
  {
    created_at: 1590969599,
    products: [
      {
        image: image1,
        product_name: 'BeoPlay Speaker',
        id: 1,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2300,
      },
      {
        image: image2,
        product_name: 'Smart BeoPlay Speaker',
        id: 2,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 1200,
      },
      {
        image: image3,
        product_name: 'Smart Bluetooth Speaker',
        id: 3,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 2000,
      },
      {
        image: image4,
        product_name: 'Bluetooth Speaker',
        id: 4,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2500,
      },
    ],
    order_name: 'OH - 424923191 - K',
    price: 1500,
    status: 'Delivered',
  },
  {
    created_at: 1588291200,
    products: [
      {
        image: image1,
        product_name: 'BeoPlay Speaker',
        id: 1,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2300,
      },
      {
        image: image2,
        product_name: 'Smart BeoPlay Speaker',
        id: 2,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 1200,
      },
      {
        image: image3,
        product_name: 'Smart Bluetooth Speaker',
        id: 3,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 2000,
      },
    ],
    order_name: 'OH - 424923191 - K',
    price: 1500,
    status: 'Delivered',
  },
  {
    created_at: 1585699200,
    products: [
      {
        image: image1,
        product_name: 'BeoPlay Speaker',
        id: 1,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2300,
      },
      {
        image: image2,
        product_name: 'Smart BeoPlay Speaker',
        id: 2,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Smart Inc',
        cost: 1200,
      },
    ],
    order_name: 'OH - 424923191 - K',
    price: 1500,
    status: 'Delivered',
  },
  {
    created_at: 1588291199,
    products: [
      {
        image: image1,
        product_name: 'BeoPlay Speaker',
        id: 1,
        product_size: 'XL',
        product_color: '#33427d',
        product_info:
          'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
        brand: 'Google LLC',
        cost: 2300,
      },
    ],
    order_name: 'OH - 424923191 - K',
    price: 1500,
    status: 'Delivered',
  },
];

export const profile = {
  image: avatar1,
  user_name: 'David Spade',
  email: 'pipong@gmail.com',
};

export const products = [
  {
    image: image1,
    product_name: 'BeoPlay Speaker',
    id: 1,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Google LLC',
    cost: 2300,
  },
  {
    image: image2,
    product_name: 'Smart BeoPlay Speaker',
    id: 2,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 1200,
  },
  {
    image: image3,
    product_name: 'Smart Bluetooth Speaker',
    id: 3,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image4,
    product_name: 'Bluetooth Speaker',
    id: 4,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Google LLC',
    cost: 2500,
  },
  {
    image: image5,
    product_name: 'Smart Bluetooth Speaker',
    id: 5,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Google LLC',
    cost: 1500,
  },
  {
    image: image6,
    product_name: 'Wireless Remote',
    id: 6,
    product_size: 'XL',
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 1100,
  },
];

export const reviews = [
  {
    image: avatar1,
    reviewer_name: 'BeoPlay Speaker',
    review_content:
      'Wonderful jean, perfect gift for my girl for our anniversary!',
    rate: 5,
    product_id: 1,
  },
  {
    image: avatar2,
    reviewer_name: 'BeoPlay Speaker',
    review_content:
      'I love this, paired it with a nice blouse and all eyes on me.',
    rate: 5,
    product_id: 1,
  },
  {
    image: avatar1,
    reviewer_name: 'BeoPlay Speaker',
    review_content:
      'Wonderful jean, perfect gift for my girl for our anniversary!',
    rate: 5,
    product_id: 1,
  },
  {
    image: avatar2,
    reviewer_name: 'BeoPlay Speaker',
    review_content:
      'I love this, paired it with a nice blouse and all eyes on me.',
    rate: 5,
    product_id: 1,
  },
];

const brand1 = require('assets/images/brands/brand1.png');
const brand2 = require('assets/images/brands/brand2.png');
const brand3 = require('assets/images/brands/brand3.png');

export const brands = [
  {
    image: brand1,
    brand_name: 'Apple',
    id: 1,
    product_total: 12340,
  },
  {
    image: brand2,
    brand_name: 'B&o',
    id: 2,
    product_total: 2000,
  },
  {
    image: brand3,
    brand_name: 'Beats',
    id: 3,
    product_total: 3200,
  },
];

export const colors = [
  {
    name: 'white',
    hex: '#aeb8df',
  },
  {
    name: 'white',
    hex: '#33427d',
  },
  {
    name: 'white',
    hex: '#ff3d00',
  },
  {
    name: 'white',
    hex: '#ddd',
  },
  {
    name: 'white',
    hex: '#00c569',
  },
  {
    name: 'white',
    hex: '#c92636',
  },
  {
    name: 'white',
    hex: '#fff',
  },
];

export const cartProducts = [
  {
    image: image1,
    product_name: 'BeoPlay Speaker',
    id: 1,
    product_size: 'XL',
    product_color: '#33427d',
    amount: 3,
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Google LLC',
    cost: 2300,
  },
  {
    image: image2,
    product_name: 'Smart BeoPlay Speaker',
    id: 2,
    product_size: 'XL',
    amount: 2,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 1200,
  },
  {
    image: image3,
    product_name: 'Smart Bluetooth Speaker',
    id: 3,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image4,
    product_name: 'Smart Bluetooth Speaker (1)',
    id: 4,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image5,
    product_name: 'Smart Bluetooth Speaker (2)',
    id: 5,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image6,
    product_name: 'Smart Bluetooth Speaker (3)',
    id: 6,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image6,
    product_name: 'Smart Bluetooth Speaker (4)',
    id: 7,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
  {
    image: image3,
    product_name: 'Smart Bluetooth Speaker (5)',
    id: 8,
    product_size: 'XL',
    amount: 1,
    product_color: '#33427d',
    product_info:
      'Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer. Nike Dri-FIT is a polyester fabric designed to help you keep dry so you can more comfortably work harder, longer',
    brand: 'Smart Inc',
    cost: 2000,
  },
];

const icon_men = require('assets/images/category/Icon_shoe.png');
const icon_women = require('assets/images/category/Icon_women_shoe.png');
const icon_devices = require('assets/images/category/Icon_devices.png');
const icon_headphone = require('assets/images/category/Icon_headphone.png');
const icon_gaming = require('assets/images/category/Icon_gaming.png');

export const categories = [
  {image: icon_men, title: 'Men', id: 1},
  {image: icon_women, title: 'Women', id: 2},
  {image: icon_devices, title: 'Devices', id: 3},
  {image: icon_headphone, title: 'Gadgets', id: 4},
  {image: icon_gaming, title: 'Gaming', id: 5},
];
