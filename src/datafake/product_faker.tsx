import faker from 'faker';

const data = [];
const count = 200;

let range = (n) => Array.from(Array(n).keys());

for (i in range(count)) {
  data.push({
    id: faker.random.number(),
    product: {
      color: faker.commerce.color(),
      product_name: faker.commerce.productName(),
      price: faker.commerce.price(),
      product_material: faker.commerce.productMaterial(),
    },
  });
}

export default data;
