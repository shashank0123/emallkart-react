import * as React from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as cartSelectors from 'screens/Pages/Cart/selectors';
import {rateHeight} from 'services/DeviceInfo';
function IconWithBadge({name, badgeCount, color, size}) {
  return (
    <View style={{width: 24, height: 24, margin: 5}}>
      <MaterialCommunityIcons name={name} size={size} color={color} />
      {badgeCount > 0 && (
        <View
          style={{
            // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: '#00c569',
            borderRadius: 20 * rateHeight,
            width: 14 * rateHeight,
            height: 14 * rateHeight,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>
            {badgeCount}
          </Text>
        </View>
      )}
    </View>
  );
}
class HomeIconWithBadge extends React.PureComponent<Props> {
  _getTotalProducts = () => {
    const {cartProducts} = this.props;
    let total = 0;
    cartProducts.forEach((item) => {
      total += item.amount;
    });
    return total;
  };
  render() {
    return (
      <IconWithBadge {...this.props} badgeCount={this._getTotalProducts()} />
    );
  }
}

/* @todo using :any */
const mapStateToProps = (state: any) => ({
  cartProducts: cartSelectors.getCartProducts(state),
});

export default connect(mapStateToProps, {})(HomeIconWithBadge);
