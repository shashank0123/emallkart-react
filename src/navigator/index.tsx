import * as React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import * as Routers from './routerName';
import WellcomeScreen from 'screens/Pages/Authentication/Wellcome';
import SignUpScreen from 'screens/Pages/Authentication/SignUp';
import VerificationScreen from 'screens/Pages/Authentication/Verification';
import SplashScreen from 'screens/Pages/Launch/main';
import ExploreScreen from 'screens/Pages/Explore/Main';
import CategoryScreen from 'screens/Pages/Explore/Category';
import BrandScreen from 'screens/Pages/Explore/Brand';
import ProductScreen from 'screens/Pages/Explore/Product';
import WriteReview from 'screens/Pages/Explore/WriteReview';
import FilterReview from 'screens/Pages/Explore/Filter';
import CameraScreen from 'screens/Pages/Explore/Camera';

import ProductList from 'screens/Pages/Cart/ProductList';
import CheckoutStep1 from 'screens/Pages/Cart/CheckoutStep1';
import CheckoutStep2 from 'screens/Pages/Cart/CheckoutStep2';
import CheckoutStep3 from 'screens/Pages/Cart/CheckoutStep3';
import CheckoutStep4 from 'screens/Pages/Cart/CheckoutStep4';

import AccountMenuScreen from 'screens/Pages/Account/Menu';
import TrackOrder from 'screens/Pages/Account/TrackOrder';
import OrderProcess from 'screens/Pages/Account/OrderProcess';
import ShipAddress from 'screens/Pages/Account/ShipAddress';

import {rateHeight, rateWidth, isDeviceIphoneX} from 'services/DeviceInfo';
import HomeIconWithBadge from './badge';
const Stack = createStackNavigator();
// Authen
function AuthenticationStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={Routers.AUTH_WELLCOME} component={WellcomeScreen} />
      <Stack.Screen name={Routers.AUTH_SIGNUP} component={SignUpScreen} />
      <Stack.Screen
        name={Routers.AUTH_VERIFICATION}
        component={VerificationScreen}
      />
    </Stack.Navigator>
  );
}
// Top screens
function TopStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={Routers.EXPRORE_MAIN} component={ExploreScreen} />
      {/* TODO */}
    </Stack.Navigator>
  );
}

function CartStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={Routers.CART_CHECKOUT} component={ProductList} />
      <Stack.Screen
        name={Routers.CART_CHECKOUT_STEP1}
        component={CheckoutStep1}
      />
      <Stack.Screen
        name={Routers.CART_CHECKOUT_STEP2}
        component={CheckoutStep2}
      />
      <Stack.Screen
        name={Routers.CART_CHECKOUT_STEP3}
        component={CheckoutStep3}
      />
      <Stack.Screen
        name={Routers.CART_CHECKOUT_STEP4}
        component={CheckoutStep4}
      />
    </Stack.Navigator>
  );
}

function AccountStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={Routers.ACCOUNT_INFO} component={AccountMenuScreen} />
      {/* TODO */}
    </Stack.Navigator>
  );
}

function AccountItemStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={Routers.ACCOUNT_TRACK_ORDER} component={TrackOrder} />
      <Stack.Screen
        name={Routers.ACCOUNT_TRACK_ORDER_PROCESS}
        component={OrderProcess}
      />
      <Stack.Screen
        name={Routers.ACCOUNT_SHIP_ADDRESS}
        component={ShipAddress}
      />
      {/* TODO */}
    </Stack.Navigator>
  );
}

function ExploreStack() {
  return (
    <Stack.Navigator
      mode="card"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name={Routers.EXPRORE_CATEGORY}
        component={CategoryScreen}
      />
      <Stack.Screen name={Routers.EXPRORE_BRAND} component={BrandScreen} />
      <Stack.Screen name={Routers.EXPRORE_PRODUCT} component={ProductScreen} />
      <Stack.Screen name={Routers.EXPRORE_REVIEW} component={WriteReview} />
      <Stack.Screen name={Routers.EXPRORE_FILTER} component={FilterReview} />
      <Stack.Screen name={Routers.EXPRORE_CAMERA} component={CameraScreen} />
      {/* TODO */}
    </Stack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

function TabStack() {
  return (
    <Tab.Navigator
      mode="card"
      tabBarOptions={{
        style: {
          height: isDeviceIphoneX() ? 84 * rateHeight : 60 * rateHeight,
        },
        activeTintColor: '#00c569',
      }}>
      <Tab.Screen
        name={Routers.EXPRORE_MAIN}
        component={TopStack}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="home"
              color={color}
              size={26 * rateWidth}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Routers.CART}
        component={CartStack}
        options={{
          tabBarLabel: 'Cart',
          tabBarVisible: false,
          tabBarIcon: ({color}) => (
            <HomeIconWithBadge name="cart" size={26} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={Routers.ACCOUNT}
        component={AccountStack}
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
      {/* TODO */}
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={Routers.SPLASH}
        mode="card"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={Routers.SPLASH} component={SplashScreen} />
        <Stack.Screen
          name={Routers.AUTHENTICATION}
          component={AuthenticationStack}
        />
        <Stack.Screen name={Routers.EXPRORE} component={ExploreStack} />
        <Stack.Screen name={Routers.BOTTOM_TAB} component={TabStack} />
        <Stack.Screen
          name={Routers.ACCOUNT_MENU}
          component={AccountItemStack}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
