import {fork} from 'redux-saga/effects';
import ScreenSagas from './screens/sagas';

export default function* rootSaga() {
  yield fork(ScreenSagas);
}
