import { Platform } from 'react-native'
// android

export const unitId = Platform.OS === 'ios' ? 'ca-app-pub-8394432247836009/5862365185' : 'ca-app-pub-8394432247836009/6053936877'

export const interstitialId = Platform.OS === 'ios' ? 'ca-app-pub-8394432247836009/4222529199' : 'ca-app-pub-8394432247836009/6285871165'

export const rewardedId = Platform.OS === 'ios' ? 'ca-app-pub-8394432247836009/1871698021' : 'ca-app-pub-8394432247836009/1033544489'