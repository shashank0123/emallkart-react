const fonts = {
  system: 'System',
  light: 'Exo-Light',
  bold: 'Exo-Bold',
  segoeUI: 'Segoe UI',
  laTo: 'Lato',
};
export default fonts;
