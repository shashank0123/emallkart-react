import { Platform, StyleSheet } from 'react-native'
import { rateHeight, rateWidth } from 'services/DeviceInfo'

const androidStyles = StyleSheet.create({
  shadowButtonStyle: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    
    elevation: 11,
  }
})

const iosStyles = StyleSheet.create({
  shadowButtonStyle: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    
    elevation: 11,
  }
})

export default Platform.select({
  ios: () => iosStyles,
  android: () => androidStyles
})()