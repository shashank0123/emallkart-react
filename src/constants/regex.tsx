export const REGEX_PASSWORD = /^(?=.*[a-z])(?=.*\d).{6,20}$/ // ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,10}$
export const REGEX_NAME = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/
// export const REGEX_EMAIL = /^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}$/
export const REGEX_EMAIL = /^[a-zA-Z0-9.-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,61}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/