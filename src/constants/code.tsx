export const EMAIL_ALREADY_IN_USE = 'auth/email-already-in-use'
export const NETWORK_REQUEST_FAILED = 'auth/network-request-failed'
export const INVALID_CREDENTIAL = 'auth/invalid-credential'
export const INVALID_EMAIL = 'auth/invalid-email'
export const OTHER_ERROR = 'Error'

// login
export const USER_NOT_FOUND = 'auth/user-not-found'
export const WRONG_PASSWORD = 'auth/wrong-password'