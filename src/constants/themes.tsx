

import { rateHeight } from 'services/DeviceInfo'

export const light = {
  roundness: 4, // roundness of common elements, such as buttons.
  colors: {
    primary: '#2699FB', // primary color for your app, usually your brand color.
    secondary: '#FFFFFF', // secondary color for your app which complements the primary color.
    background: '#FFFFFF', // background color for pages, such as lists.
    text: '#2699FB', // text color for content.
    textBackdrop: '#FFFFFF', // text color for content.
    textPlaceholder: '#ACD9FF', // text color for content.
    disabled: '#2699FB', // color for disabled elements.
    placeholder: '#F1F9FF', // color for placeholder text, such as input placeholder.
    surface: '#2699FB', // background color for elements containing content, such as cards.
    backdrop: '#FFFFFF', // color for backdrops of various components such as modals.,
    alert: '#E22828'
  },
  font: { // various fonts used throughout different elements.
    family: 'System',
    size_small: 12 * rateHeight,
    size_medium: 14 * rateHeight,
    size_large: 16 * rateHeight,
    size_header: 18 * rateHeight,
    line_height: 17 * rateHeight,
    weight_light: '200',
    weight_normal: 'normal',
    weight_semi_bold: '600',
    weight_bold: '800'
  }
}

export const dark = {
  roundness: 4, // roundness of common elements, such as buttons.
  colors: {
    primary: '#000000', // primary color for your app, usually your brand color.
    secondary: '#2699FB', // secondary color for your app which complements the primary color.
    background: '#000000', // background color for pages, such as lists.
    text: '#000000', // text color for content.
    textBackdrop: '#2699FB', // text color for content.
    disabled: '#000000', // color for disabled elements.
    placeholder: '#000000', // color for placeholder text, such as input placeholder.
    surface: '#000000', // background color for elements containing content, such as cards.
    backdrop: '#FFFFFF' // color for backdrops of various components such as modals.
  },
  font: { // various fonts used throughout different elements.
    family: 'System',
    size_small: 12 * rateHeight,
    size_medium: 14 * rateHeight,
    size_large: 16 * rateHeight,
    line_height: 17 * rateHeight,
    weight_light: '200',
    weight_normal: 'normal',
    weight_semi_bold: '600',
    weight_bold: '800'
  }
}