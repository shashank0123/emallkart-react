export const THIS_WEEK = 0
export const ONE_DAY = 1
export const BREAKFAST = 2
export const LUNCH = 3
export const DINNER = 4
export const SNACK = 5


export const NAME = 'name'
export const TIME = 'time'
export const CALORIE = 'calorie'
export const PROTEIN = 'protein'
export const LIPID = 'lipid'
export const CARBOHYDRATE = 'carbohydrate'
export const SALTEQUIVALENT = 'salt_equivalent'
export const KCAL = 'kcal'
export const G = 'g'
export const STATUS_IDLE = 'STATUS_IDLE'
export const STATUS_SUCCESS = 'STATUS_SUCCESS'
export const STATUS_ERROR = 'STATUS_ERROR'
export const STATUS_PROCESSING = 'STATUS_PROCESSING'
export const LIMIT_NAME = 30
export const LIMIT_NUMBER = 6