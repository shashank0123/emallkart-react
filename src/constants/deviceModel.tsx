// import DeviceInfo from 'react-native-device-info'

export const IPHONE_4 = [
  'iPhone 4',
  'iPhone 4s'
]
export const IPHONE_5 = [
  'iPhone 5',
  'iPhone 5s',
  'iPhone 5c',
  'iPhone SE'
]
export const IPHONE_6 = [
  'iPhone 6',
  'iPhone 6s',
  'iPhone 6s Plus',
  'iPhone 6 Plus'
]
export const IPHONE_7 = [
  'iPhone 7',
  'iPhone 7 Plus'
]
export const IPHONE_8 = [
  'iPhone 8',
  'iPhone 8 Plus'
]
export const IPHONE_X = [
  'iPhone Xs',
  'iPhone XS',
  'iPhone X',
  'iPhone Xs Max',
  'iPhone XS MAX',
  'iPhone Xr',
  'iPhone XR'
]