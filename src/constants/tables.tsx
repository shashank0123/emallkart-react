
export const ID_INSERT_MEAL = 0

export const PROFILE = {
  USER_ID: 'user_id', // user id return when user register
  PASSWORD: 'password', // user id return when user register
  FCM_TOKEN: 'fcm_token_id', // fcm token of device
  AGE: 'age', // date of birth
  GENDER: 'gender', // sex 0 - none 1 - women 2 - man
  USER_NAME: 'name',
  SCORE: 'score',
  EMAIL: 'email',
  AVATAR: 'avatar',
  IS_DELETED: 'is_deleted',
  IS_CREATED: 'is_created',
  CREATE_DATE: 'create_date',
}

export const GROUP = {
  GROUP_ID: 'group_id',
  QUESTION_INDEX: 'question_index',
  USER_ID: 'user_id', // user id return when user register
  CREATED_AT: 'createdAt', // user id return when user register
  PARTNER_AVATAR: 'partner_avatar', // fcm token of device
  PARTNER_ID: 'partner_id', // date of birth
  USER_AVATAR: 'user_avatar', // sex 0 - none 1 - women 2 - man
  USER_NAME: 'user_name',
  PARTNER_NAME: 'partner_name',
  SCORE: 'score',
  PARTNER_STATUS: 'partner_status',
  USER_STATUS: 'user_status',
  USER_SCORE: 'user_score',
  PARTNER_SCORE: 'partner_score',
  GAME_STATUS: 'status'
}