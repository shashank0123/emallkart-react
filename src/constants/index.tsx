export const FACEBOOK = 'FACEBOOK';
export const FLASH_MODE = {
  ON: 'on',
  OFF: 'off',
};

export const TYPE_MODE = {
  BACK: 'back',
  FRONT: 'front',
};
