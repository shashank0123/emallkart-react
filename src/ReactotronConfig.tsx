import {NativeModules} from 'react-native';
import {reactotronRedux} from 'reactotron-redux';
import Reactotron, {trackGlobalErrors} from 'reactotron-react-native';
import sagaPlugin from 'reactotron-redux-saga';

console.disableYellowBox = true;
// eslint-disable-next-line no-undef
if (__DEV__) {
  // swizzle the old one
  const yeOldeConsoleLog = console.log;

  // make a new one
  console.log = (...args) => {
    // always call the old one, because React Native does magic swizzling too
    yeOldeConsoleLog(...args);

    // send this off to Reactotron.
    Reactotron.display({
      name: 'CONSOLE.LOG',
      value: args,
      preview: args.length > 0 && typeof args[0] === 'string' ? args[0] : null,
    });
  };

  console.tron = Reactotron;

  // Clear previous runtime's log
  Reactotron.clear();
}

const {scriptURL} = NativeModules.SourceCode;
const scriptHostname = scriptURL.split('://')[1].split(':')[0];
export default Reactotron.configure({name: 'ShopApp', host: scriptHostname})
  .use(sagaPlugin())
  .use(
    reactotronRedux({
      except: ['REQUEST_PROCESSING', 'REQUEST_SUCCESS', 'SHOW_LOADING'],
    }),
  )
  .useReactNative({
    // storybook: true
  })
  .use(trackGlobalErrors());
