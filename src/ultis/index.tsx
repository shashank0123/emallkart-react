export const assignArrayToString = (list: array, properties: string) => {
  let result = '';
  list.forEach((item) => {
    if (item.is_selected) {
      result += item[properties];
      result += ', ';
    }
  });
  return result.substring(0, result.length - 2);
};
